-- MySQL dump 10.13  Distrib 5.6.23, for osx10.8 (x86_64)
--
-- Host: localhost    Database: bet_auction
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auction_admin`
--

DROP TABLE IF EXISTS `auction_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(80) DEFAULT NULL,
  `passkey` varchar(80) DEFAULT NULL,
  `privilege` tinyint(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction_admin`
--

LOCK TABLES `auction_admin` WRITE;
/*!40000 ALTER TABLE `auction_admin` DISABLE KEYS */;
INSERT INTO `auction_admin` VALUES (1,'oadetimehin@terragonltd.com','2460e0673a63b5d83c714bc8c08192a0',1,'2015-09-06 19:12:07','2015-09-06 19:12:07');
/*!40000 ALTER TABLE `auction_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction_original_tw`
--

DROP TABLE IF EXISTS `auction_original_tw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction_original_tw` (
  `orig_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_str` varchar(200) DEFAULT NULL,
  `tw_text` varchar(200) DEFAULT NULL,
  `source` varchar(45) DEFAULT NULL,
  `user_id_str` varchar(45) DEFAULT NULL,
  `user_screen_name` varchar(45) DEFAULT NULL,
  `retweet_count` varchar(45) DEFAULT NULL,
  `created_on` varchar(45) DEFAULT NULL,
  `time_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`orig_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction_original_tw`
--

LOCK TABLES `auction_original_tw` WRITE;
/*!40000 ALTER TABLE `auction_original_tw` DISABLE KEYS */;
/*!40000 ALTER TABLE `auction_original_tw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction_participants`
--

DROP TABLE IF EXISTS `auction_participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction_participants` (
  `participant_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_str` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `screen_name` varchar(80) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `email_address` varchar(60) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `state_of_origin` varchar(20) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `description` text,
  `url` varchar(100) DEFAULT NULL,
  `followers_count` int(11) DEFAULT NULL,
  `profile_image_url` varchar(200) DEFAULT NULL,
  `friends_count` int(11) DEFAULT NULL,
  `tw_oauth_token` varchar(80) DEFAULT NULL,
  `tw_oauth_token_secret` varchar(80) DEFAULT NULL,
  `complete_status` tinyint(4) DEFAULT '0',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`participant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction_participants`
--

LOCK TABLES `auction_participants` WRITE;
/*!40000 ALTER TABLE `auction_participants` DISABLE KEYS */;
INSERT INTO `auction_participants` VALUES (1,'1882469142','Adetimehin Segun','Segzpair','Lagos','oadetimehin@terragonltd.com','Male',89,'Akwa Ibom','07060514642','Smart developer :: UX Expert :: Big Data Analyst','http://t.co/elX4CZ59H5',55,'http://pbs.twimg.com/profile_images/486639876675100673/5wPJpjEc_normal.jpeg',103,'1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs','6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7',1,'2015-09-07 08:03:38');
/*!40000 ALTER TABLE `auction_participants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction_sessions`
--

DROP TABLE IF EXISTS `auction_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auction_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction_sessions`
--

LOCK TABLES `auction_sessions` WRITE;
/*!40000 ALTER TABLE `auction_sessions` DISABLE KEYS */;
INSERT INTO `auction_sessions` VALUES ('00b659d78866531e0ce047abdc5db17074a2c86f','::1',1441610566,'__ci_last_regenerate|i:1441610397;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('06d614c8707b93866f6c38021f107afa4862a1e9','::1',1441600280,'__ci_last_regenerate|i:1441600092;tw_oauth_token|s:27:\"fzq-SgAAAAAAhaV6AAABT6YQLS4\";tw_oauth_token_secret|s:32:\"D6kZWGk7D1bEHc8Kw4ulbaGGZs1U1Qng\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}'),('0a5bba35504cc70e5d118550586c93e557d06753','::1',1441609102,'__ci_last_regenerate|i:1441608803;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('0aaa2fd8868327310d0a74314505771817eec915','::1',1441614263,'__ci_last_regenerate|i:1441614029;tw_oauth_token|s:27:\"d1ANqwAAAAAAhaV6AAABT6bmJ60\";tw_oauth_token_secret|s:32:\"sAkLreF05MkcaMHVVUYt0tsi1BWq0tmF\";'),('0cb8e8f4b3b213dd5ea418cc6c58984cc7db77a2','::1',1441615561,'__ci_last_regenerate|i:1441615435;tw_oauth_token|s:27:\"kkxfCQAAAAAAhaV6AAABT6b59xQ\";tw_oauth_token_secret|s:32:\"S9jVxPe83gV94w6n8NjBAmmV3DHhudsh\";'),('0f06ec93804a5b2926abab09cb094c98666d37d6','::1',1441602526,'__ci_last_regenerate|i:1441602236;tw_oauth_token|s:27:\"nrrFLgAAAAAAhaV6AAABT6YzFDA\";tw_oauth_token_secret|s:32:\"EPcyevslIQwddGchlUJk5258X35dFvJP\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('1404cd0a4c6772ebfa97309fd3b90e053bb65b78','::1',1441600090,'__ci_last_regenerate|i:1441599788;tw_oauth_token|s:27:\"USIoDwAAAAAAhaV6AAABT6YN6fo\";tw_oauth_token_secret|s:32:\"M21ApIOAnIpVhJjN5g3LvrktgGirikgS\";'),('1d85b66721207777416e02992d4012ae617d049d','::1',1441603680,'__ci_last_regenerate|i:1441603419;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('210c9cf3e5e600e2e2f78d98ca3bf5bd4a64b20e','::1',1441606920,'__ci_last_regenerate|i:1441606816;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('217cc8ab60e5920363671ea5fd220ee780f3b2be','::1',1441612989,'__ci_last_regenerate|i:1441612702;tw_oauth_token|s:27:\"Rhj4QQAAAAAAhaV6AAABT6bSudU\";tw_oauth_token_secret|s:32:\"rLEpu3miLMfo7jFtdHui5f7tJnwdnVl1\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('264e60588985e91aa24c8d99d0e0c7d1442b4547','::1',1441610010,'__ci_last_regenerate|i:1441609754;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('312024e9c85225e3497e3b6069b082fe0aec90a6','::1',1441593652,'__ci_last_regenerate|i:1441593555;tw_oauth_token|s:27:\"H9b18wAAAAAAhaV6AAABT6WDn6k\";tw_oauth_token_secret|s:32:\"YLXo2QnzwIbW2fQdP9Qt0tP71ahMZ68N\";'),('320abb27c45aaa779796b17a430f4ae025c6c0e5','::1',1441597822,'__ci_last_regenerate|i:1441597534;tw_oauth_token|s:27:\"YhZfQQAAAAAAhaV6AAABT6XnYfc\";tw_oauth_token_secret|s:32:\"14xWIVdB38v9ZZlr5hveN0vfxRfkfg5i\";'),('32fe786dbe7007fdd261cda3fa66cc2d7cd18382','::1',1441606213,'__ci_last_regenerate|i:1441605949;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('32ff790178228e4eee32ce9da318d2a6862f26ef','::1',1441602685,'__ci_last_regenerate|i:1441602612;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('3883c0000f2cf4a907c0fc606d7bdd2de160db58','::1',1441607310,'__ci_last_regenerate|i:1441607251;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('3bcc75003b6b9970922b9df16b56b391bf5fd7a6','::1',1441610084,'__ci_last_regenerate|i:1441610082;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('4720ec8a82c3e9c468a91987fd3bdefd5cae11b9','::1',1441596940,'__ci_last_regenerate|i:1441596909;tw_oauth_token|s:27:\"h3P40wAAAAAAhaV6AAABT6Xd17Q\";tw_oauth_token_secret|s:32:\"KmZ2l7eDhNXtgV9FsRLs6BmlIR5EH5Wt\";'),('4cf759787c5beef12fd5f17c4c1689e268718f98','::1',1441599052,'__ci_last_regenerate|i:1441598942;tw_oauth_token|s:27:\"MgugnQAAAAAAhaV6AAABT6X854o\";tw_oauth_token_secret|s:32:\"SwXE824VAaxRDBqhcIVNyVWJENBKJuZi\";'),('5e46f887a769ed9b9555e0aa80a4844760249a86','::1',1441608666,'__ci_last_regenerate|i:1441608383;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('69659208221f8d1af27875e7fbb20f1a84249bfe','::1',1441616272,'__ci_last_regenerate|i:1441615982;tw_oauth_token|s:27:\"T59fEQAAAAAAhaV6AAABT6cEz_o\";tw_oauth_token_secret|s:32:\"fQduQkhlREK60fzEhJ41wU4yQw8Ak08a\";'),('6da6952b39255a84d106ba61b51d0077aa9ece33','::1',1441603910,'__ci_last_regenerate|i:1441603910;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('7e61f369f32c93a8ad426cc165988f29e2659c69','::1',1441611720,'__ci_last_regenerate|i:1441611719;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('7fb12ddd696c664be28743a765c135c86bcc7d8c','::1',1441613848,'__ci_last_regenerate|i:1441613848;tw_oauth_token|s:27:\"Rhj4QQAAAAAAhaV6AAABT6bSudU\";tw_oauth_token_secret|s:32:\"rLEpu3miLMfo7jFtdHui5f7tJnwdnVl1\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('8416a9ec936a69f58043af913f041537c1f48f70','::1',1441612603,'__ci_last_regenerate|i:1441612359;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('8648dbd577d743843cb5c66302a0083bbb0670e5','::1',1441609359,'__ci_last_regenerate|i:1441609245;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('91f5b9ed5961a88df40b6248fe0c1d1fb0e95f4a','::1',1441616347,'__ci_last_regenerate|i:1441616306;tw_oauth_token|s:27:\"BiXUVwAAAAAAhaV6AAABT6cF89w\";tw_oauth_token_secret|s:32:\"YcqBVxhAbfNF2NyaGdcnvtJx3rEwm0kD\";'),('9353b5f850e07bf9018e058efb0ebed7fb08ec4a','::1',1441613246,'__ci_last_regenerate|i:1441613013;tw_oauth_token|s:27:\"Rhj4QQAAAAAAhaV6AAABT6bSudU\";tw_oauth_token_secret|s:32:\"rLEpu3miLMfo7jFtdHui5f7tJnwdnVl1\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('ab55340384b1788d907a8061eefcbc4b99ebd07f','::1',1441594061,'__ci_last_regenerate|i:1441594061;tw_oauth_token|s:27:\"H9b18wAAAAAAhaV6AAABT6WDn6k\";tw_oauth_token_secret|s:32:\"YLXo2QnzwIbW2fQdP9Qt0tP71ahMZ68N\";'),('b4b25db12ff11d94643183263f189e373290b64a','::1',1441596231,'__ci_last_regenerate|i:1441595958;tw_oauth_token|s:27:\"H9b18wAAAAAAhaV6AAABT6WDn6k\";tw_oauth_token_secret|s:32:\"YLXo2QnzwIbW2fQdP9Qt0tP71ahMZ68N\";'),('b6a9b4c84788a9503896924a4262f82c5358ae09','::1',1441606593,'__ci_last_regenerate|i:1441606361;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('c10b20298046e89c55a12f07e8dd50096a9e4dc5','::1',1441597960,'__ci_last_regenerate|i:1441597958;tw_oauth_token|s:27:\"YhZfQQAAAAAAhaV6AAABT6XnYfc\";tw_oauth_token_secret|s:32:\"14xWIVdB38v9ZZlr5hveN0vfxRfkfg5i\";'),('c38792e916c8f140cc12e072b7a6306ca9c6144b','::1',1441613692,'__ci_last_regenerate|i:1441613423;tw_oauth_token|s:27:\"Rhj4QQAAAAAAhaV6AAABT6bSudU\";tw_oauth_token_secret|s:32:\"rLEpu3miLMfo7jFtdHui5f7tJnwdnVl1\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('cc4383aab024b2fc64d6bef081654f49cca98a4a','::1',1441596514,'__ci_last_regenerate|i:1441596261;tw_oauth_token|s:27:\"H9b18wAAAAAAhaV6AAABT6WDn6k\";tw_oauth_token_secret|s:32:\"YLXo2QnzwIbW2fQdP9Qt0tP71ahMZ68N\";'),('d0e0786d15dae5fb571b4662c39b35b9c1e512d3','::1',1441611051,'__ci_last_regenerate|i:1441610832;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('d91014c6979cde36232281d49777ca9e0115b147','::1',1441600092,'__ci_last_regenerate|i:1441600090;tw_oauth_token|s:27:\"CCG91QAAAAAAhaV6AAABT6YN8sU\";tw_oauth_token_secret|s:32:\"z4xaZa1PNGKUQlFYEY8QmGNYzAxKqYvb\";'),('ddbef3cf8e96fc2cfe9161fe5293c8372e393840','::1',1441593203,'__ci_last_regenerate|i:1441593201;tw_oauth_token|s:27:\"H9b18wAAAAAAhaV6AAABT6WDn6k\";tw_oauth_token_secret|s:32:\"YLXo2QnzwIbW2fQdP9Qt0tP71ahMZ68N\";'),('e381af497698758f9aed929623d6f12bb7622ecd','::1',1441602107,'__ci_last_regenerate|i:1441601858;tw_oauth_token|s:27:\"XF1Q3gAAAAAAhaV6AAABT6YssT4\";tw_oauth_token_secret|s:32:\"cMHhFGn8oj28KIzc9v29SyQ4RcNP1Eqm\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}'),('f1eff00295f77516bda7c2c1ac0cff61cf73457d','::1',1441605774,'__ci_last_regenerate|i:1441605648;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('f36f99e076cae8addff71c2df959df12a423d4f2','::1',1441608000,'__ci_last_regenerate|i:1441607923;tw_oauth_token|s:27:\"_eP50QAAAAAAhaV6AAABT6Y0ayI\";tw_oauth_token_secret|s:32:\"xwswW4sAHkmMi6wdHgAZC9PtwGljJAx5\";tw_access_token|a:5:{s:11:\"oauth_token\";s:50:\"1882469142-Ri0celxGIjAH9Ug052AJgeOGrHAjeKpLnp8oAUs\";s:18:\"oauth_token_secret\";s:45:\"6m2gAYGHlwHKzX1ucmcXYKG1feUE3GkDnYPcj4uSJkAo7\";s:7:\"user_id\";s:10:\"1882469142\";s:11:\"screen_name\";s:8:\"Segzpair\";s:14:\"x_auth_expires\";s:1:\"0\";}participant_identity|s:1:\"1\";'),('f44624812043297ed6bcaac3b14d0888b5e5ba2a','::1',1441598439,'__ci_last_regenerate|i:1441598437;tw_oauth_token|s:27:\"YhZfQQAAAAAAhaV6AAABT6XnYfc\";tw_oauth_token_secret|s:32:\"14xWIVdB38v9ZZlr5hveN0vfxRfkfg5i\";');
/*!40000 ALTER TABLE `auction_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction_settings`
--

DROP TABLE IF EXISTS `auction_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(60) DEFAULT NULL,
  `value` text,
  `field_type` varchar(20) DEFAULT NULL,
  `info_type` text,
  `readonly_status` tinyint(1) DEFAULT '0',
  `item_order` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction_settings`
--

LOCK TABLES `auction_settings` WRITE;
/*!40000 ALTER TABLE `auction_settings` DISABLE KEYS */;
INSERT INTO `auction_settings` VALUES (1,'SEARCH_QUERY','#GalaxyATweetBid','text','characters',0,NULL),(2,'CONSUMER_KEY','xSbq5tjPchK4Jd2jo4B8NbJxb','text','characters',1,NULL),(3,'CONSUMER_SECRET','ofcuDLcAtzcqxx2gUMRzzb7Om1zKbjeTiRvCoL9bhKPvJxcyKZ','text','characters',1,NULL),(4,'ACCESS_TOKEN','1882469142-397M1RHnSYvXLqStG8pbjg5xk2wblgBc2IustjY','text','characters',1,NULL),(5,'ACCESS_TOKEN_SECRET','Z4IeMP8XKXZBKdjp7ClGcTJdZHqXoeGQpLwtCWYpoEPxp','text','characters',1,NULL),(6,'RECEIVING_EMAIL','oadetimehin@terragonltd.com','email','characters',0,NULL),(7,'APP_NAME','Premier Betting - Twitter Auction Competition','text','characters',0,NULL),(8,'THEME','eProject','text','characters',1,NULL),(9,'SCREEN_NAME','#Dare2Pair','text','characters',0,NULL),(10,'QUERY_COUNT','100','number','integers',0,NULL),(11,'SMTP_USER','localhost','text','characters',1,NULL),(12,'SMTP_PORT','25','number','integer',1,NULL),(13,'APP_USERNAME','twinpine','text','character',1,NULL),(14,'APP_PASSWORD','simple','password','charcter and integer',1,NULL),(15,'PAGINATOR_COUNT','100','number','integer',0,NULL),(16,'APP_DESCRIPTION','Beting platform spiced with twitter auction','text','Description of Application',0,NULL),(17,'OAUTH_CALLBACK','http://localhost/bet-auction/twitter/oauth-back/','text','Twitter oauth url',0,NULL),(18,'CAMPAIGN_ENDPOINT','http://localhost/bet-auction/competition/end-point/','text','Where campaign should be redirected to...this should be client\'s url to get information from us.',0,NULL);
/*!40000 ALTER TABLE `auction_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-07 10:51:50
