<!-- Navbar -->
<nav class="navbar-youplay navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="off-canvas" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{$BASE_URL}">
                <img src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/images/logo.png" alt="">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#"><!-- {$BASE_URL}info/how-to-participate/ -->
                        Quick Info
                        <span class="label">How to Participate</span>
                    </a>
                </li>
                <li>
                    <a href="#"><!-- {$BASE_URL}leader-board/-->
                        Leader Board
                        <span class="label">List of Winners</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Community
                        <span class="label">Forums</span>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-hover dropdown-cart">
                    <a href="javascript:void(0)" class="dropdown-toggle login-string" data-toggle="dropdown" role="button" aria-expanded="true">
                        <i class="fa fa-lock"></i> &nbsp;&nbsp;Sign In / Register
                    </a>
                    <div class="dropdown-menu" style="width: 300px;">
                        <div class="block-content col-md-12 mnb-10">
                            <div class="lwa lwa-default">
                                <form class="block-content" action="#" method="post">
                                    <p>Username:</p>
                                    <div class="youplay-input">
                                        <input type="text" name="log">
                                    </div>

                                    <p>Password:</p>
                                    <div class="youplay-input">
                                        <input type="password" name="pwd">
                                    </div>

                                    <div class="youplay-checkbox mb-15 ml-5">
                                        <input type="checkbox" name="rememberme" value="forever" id="rememberme" tabindex="103">
                                        <label for="rememberme">Remember Me</label>
                                    </div>

                                    <div class="m-r-10 m-b-10"></div>

                                    <button class="btn btn-sm ml-0 mr-0 btn-block" name="wp-submit" id="lwa_wp-submit" tabindex="100">Log In</button>

                                    <br>
                                    <p><small>Don't have an account yet...? <a href="index.html#">Register</small></a>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- /Navbar -->