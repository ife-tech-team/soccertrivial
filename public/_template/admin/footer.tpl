<div class="footer">
    <div class="row">
        <div class="col-xs-12"> <a href="http://www.batchthemes.com" target="_blank">© 2016. Batch Themes Ltd. </a><a href="http://themeforest.net/item/marino-bootstrap-4-dashboard-ui-kit/15735840" target="_blank">Buy Marino</a> </div>
    </div>
</div>
</div>
</div>
</div>
<!-- global scripts -->
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/jquery/dist/jquery.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/tether/dist/js/tether.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/bootstrap/dist/js/bootstrap.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/PACE/pace.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/lodash.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/components/jquery-fullscreen/jquery.fullscreen-min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/jquery-storage-api/jquery.storageapi.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/wow/dist/wow.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/functions.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/colors.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/left-sidebar.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/navbar.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/horizontal-navigation-1.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/horizontal-navigation-2.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/horizontal-navigation-3.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/main.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/bootstrap-validator/dist/validator.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/notifyjs/dist/notify.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/Chart.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/chartist/dist/chartist.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/jquery.easypiechart.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/d3/d3.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/topojson.min.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/datamaps.all.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/dashboards.js"></script>
<script src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/scripts/index.js"></script>
</body>

</html>