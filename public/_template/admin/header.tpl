<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{$APP_NAME}</title>
    <meta name="description" content="Marino, Admin theme, Dashboard theme, AngularJS Theme">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico">
    <!--[if IE]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- global stylesheets -->
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/styles/bootstrap.css">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/styles/font-awesome.min.css">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/styles/animate.min.css">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/styles/material-design-iconic-font.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
    <link href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/styles/lag-icon.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/styles/main.css">
    <link rel="stylesheet" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/bower_components/chartist/dist/chartist.min.css"> </head>