{include file="competition/header.tpl"}
<!-- main banner start -->
<div class="banner" id="welcome">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-7">

                <!-- logo, main heading, CTA buttons  -->
                <a href="#home" class="logo" title="Obsession"><img src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/premier-betting-auction-logo.png" width="250" alt="{$APP_NAME}"></a>
                {if $participant_profile|default:''}
                    <h1>
                        Hi <strong>{$participant_profile.name}</strong>,
                        {if $participant_profile.complete_status}
                            It's time to Win Smart.
                            <small>Click the link below to place quick bet on...</small>
                        {else}
                            Only complete profile is allowed in the competition
                            <small>Kindly update your profile below...</small>
                        {/if}
                    </h1>
                {else}
                    <h1>
                        Do you want to be <strong>Recognized</strong> and <strong>Rewarded</strong> again?
                        <small>Then Partake of {$APP_NAME}</small>
                    </h1>
                {/if}
                <div class="cta-btns">
                    {if $participant_profile|default:''}
                        {if $participant_profile.complete_status}
                            <a href="{$campaign_url}" class="btn btn-primary btn-lg download-link" title="Go Win via {$APP_NAME}">
                                <i class="fa fa-trophy"></i> Go Win a Bet!
                            </a>
                        {else}
                            <a href="{$BASE_URL}competition/finish/" class="btn btn-primary btn-lg download-link" title="Update Profile on {$APP_NAME}">
                                <i class="fa fa-cloud-upload"></i> Update Profile
                            </a>
                        {/if}
                    {else}
                        {if $tw_login_url|default:''}
                        <a href="{$tw_login_url}" class="btn btn-primary btn-lg download-link" title="Participate in {$APP_NAME}">
                            <i class="icon_cloud-download_alt"></i> Join Auction
                        </a>
                        {/if}
                    {/if}
                    <a href="#about-competition" class="btn btn-primary btn-lg btn-link learn-more" title="Learn more about {$APP_NAME}">
                        Learn More <i class="fa fa-angle-double-down"></i>
                    </a>
                </div>

            </div>
            <div class="col-sm-5 hidden-xs">
                <div class="phone-mockup style1">
                    <div class="wrapper">
                        <img class="layer-one wow fadeInLeft visible-lg" data-wow-delay="1s"
                             src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/mockups/iphone-dark.png" width="316" height="581">
                        <img class="layer-two" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/mockups/iphone-white.png"
                             width="316" height="581">
                    </div>
                </div>
                <!-- iPhone mockup style1 end -->
            </div>
        </div>

        <!-- available on -->
        <small class="available-on">{$APP_NAME} | Terms and Conditions Apply.</small>

    </div>
</div>
<!-- main banner end -->


<!-- about-compeitition section start -->
<div class="features section" id="about-competition">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2 class="h2 text-center">{$APP_NAME}</h2>
                <hr>
                <p class="lead text-center">Another opportunity to "BET" with ease and get guaranteed "WINNING".<br> Go through steps below to make your win come through....</p>
            </div>
        </div>

        <!-- spacer -->
        <hr class="spacer">

        <div class="row">
            <div class="col-md-6">

                <!-- feature style2 -->
                <div class="feature style3 wow fadeInUp" data-wow-delay="1s">
                    <i class="icon_cloud-download_alt"></i>
                    <h4>Join Auction</h4>
                    <p>Using your Twitter Social Media account. All that is required is for your approval at authentication level, only basic information will be retrieve off your account.</p>
                </div>
                <!-- feature style1 -->

            </div>
            <div class="col-md-6">

                <!-- feature style2 -->
                <div class="feature style3 wow fadeInUp" data-wow-delay="1.2s">
                    <i class="icon_search-2"></i>
                    <h4>Update Profile</h4>
                    <p>Provide some other information to fully opt into competition, afterwards, you shall b eredirected to the betting platform</p>
                </div>
                <!-- feature style1 -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <!-- feature style2 -->
                <div class="feature style3 wow fadeInUp" data-wow-delay="1s">
                    <i class="icon-globe-alt"></i>
                    <h4>Continuous Tweet and Retweet</h4>
                    <p>As you run your bet, continous tweet and retweet (in a creative way) about hashtag <strong>{$SEARCH_QUERY}</strong> on your twitter handle to give you winning leverage. </p>
                </div>
                <!-- feature style1 -->

            </div>
            <div class="col-md-6">

                <!-- feature style2 -->
                <div class="feature style3 wow fadeInUp" data-wow-delay="1.2s">
                    <i class="icon-equalizer"></i>
                    <h4>Monitor Progress</h4>
                    <p>Check our<strong><a href="{$BASE_URL}competition/dashboard/"> Dashboard</a></strong> regularly to know when to up your game on your twitter timeline. Only continous tweet and retweet about hashtag  <strong>{$SEARCH_QUERY}</strong> can keep you in the competition.</p>
                </div>
                <!-- feature style1 -->

            </div>
        </div>
    </div>
</div>
<!-- about-competition section end -->

<hr class="line">

{if $participants|default:''}
<!-- participants section start -->
<div class="team-container section" id="participants">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="h1 text-center">Our awesome little team <small>Marc & Ben worked hard together and came up with an amazing app</small></h2>

                <!-- spacer -->
                <hr class="spacer">

                <div class="row">
                    <div class="col-lg-6">

                        <!-- team member left carousel start -->
                        <div class="owl-carousel" id="team-left">
                            <div class="team-member left">
                                <a href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/team/1.jpg" title="Ben Smith" class="lightbox"><img class="member-photo" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/team/1.jpg" width="768" height="862" alt="Ben Smith"></a>
                                <div class="content">
                                    <h3 class="member-desig"><span>Designer</span> Ben Smith</h3>
                                    <p class="member-detail">Ben is lead graphic designer and have designed more than 200 stunning app s since he joined us.</p>
                                    <div class="social">
                                        <a href="javascript:void(0);" class="tool-tip" title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="LinkedIn"><i class="fa fa fa-linkedin"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="team-member left">
                                <a href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/team/3.jpg" title="Ben Smith" class="lightbox"><img class="member-photo" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/team/3.jpg" width="768" height="862" alt="Ben Smith"></a>
                                <div class="content">
                                    <h3 class="member-desig"><span>UI/UX</span> Daniel Chaudhry</h3>
                                    <p class="member-detail">Daniel is an awesome guy. He is UI/Ux expert and our right hand. He smokes too much as you can see here.</p>
                                    <div class="social">
                                        <a href="javascript:void(0);" class="tool-tip" title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="LinkedIn"><i class="fa fa fa-linkedin"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- team member left carousel end -->

                    </div>
                    <div class="col-lg-6">

                        <!-- team member right carousel start -->
                        <div class="owl-carousel" id="team-right">
                            <div class="team-member right">
                                <a href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/team/2.jpg" title="Ben Smith" class="lightbox"><img class="member-photo" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/team/2.jpg" width="768" height="862" alt="Ben Smith"></a>
                                <div class="content">
                                    <h3 class="member-desig"><span>Developer</span> Marc Eddy</h3>
                                    <p class="member-detail">Marc is founder and back-end developer. He started development in 1998 and still a developer, because he loves development.</p>
                                    <div class="social">
                                        <a href="javascript:void(0);" class="tool-tip" title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="LinkedIn"><i class="fa fa fa-linkedin"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="team-member right">
                                <a href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/team/4.jpg" title="Ben Smith" class="lightbox"><img class="member-photo" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/competition/img/team/4.jpg" width="768" height="862" alt="Ben Smith"></a>
                                <div class="content">
                                    <h3 class="member-desig"><span>QA</span> Shan Mughal</h3>
                                    <p class="member-detail">Shan is our quality assurance expert. He has 10 yeras of enterprise level applications deployment experience.</p>
                                    <div class="social">
                                        <a href="javascript:void(0);" class="tool-tip" title="Dribbble"><i class="fa fa-dribbble"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="LinkedIn"><i class="fa fa fa-linkedin"></i></a>
                                        <a href="javascript:void(0);" class="tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- team member right carousel end -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- participants section start -->
{/if}
{include file="competition/footer.tpl"}