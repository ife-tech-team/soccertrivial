<div class="sidebar-outer-wrapper">
    <div class="sidebar-inner-wrapper">


        <div class="sidebar-1">
            <!--<div class="profile">
                <button data-click="toggle-sidebar" type="button" class="btn btn-white btn-outline no-border close-sidebar"> <i class="fa fa-close"></i> </button>
                <div class="profile-image"> <img class="img-circle img-fluid" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/assets/faces/m1.png"> </div>
                <div class="social-media">
                    <button type="button" class="btn btn-facebook btn-circle m-r-5"><i class="fa fa-facebook color-white"></i> </button>
                    <button type="button" class="btn btn-twitter btn-circle m-r-5"><i class="fa fa-twitter color-white"></i> </button>
                    <button type="button" class="btn btn-google btn-circle m-r-5"><i class="fa fa-google color-white"></i> </button>
                </div>
                <div class="profile-toggle">
                    <button data-click="toggle-profile" type="button" class="btn btn-white btn-outline no-border"> <i class="pull-right fa fa-caret-down icon-toggle-profile"></i> </button>
                </div>
                <div class="profile-title">Lucas smith</div>
                <div class="profile-subtitle">lucas.smith@gmail.com</div>
            </div>-->
            <div class="sidebar-nav">
                <!--<div class="sidebar-section account-links">
                    <div class="section-title">Account</div>
                    <ul class="list-unstyled section-content">
                        <li>
                            <a class="sideline"> <i class="zmdi zmdi-account-circle md-icon pull-left"></i> <span class="title">Profile</span> </a>
                        </li>
                        <li>
                            <a class="sideline"> <i class="zmdi zmdi-settings md-icon pull-left"></i> <span class="title">Settings</span> </a>
                        </li>
                        <li>
                            <a class="sideline"> <i class="zmdi zmdi-favorite-outline md-icon pull-left"></i> <span class="title">Favorites</span> </a>
                        </li>
                        <li>
                            <a class="sideline"> <i class="zmdi zmdi-sign-in md-icon pull-left"></i> <span class="title">Logout</span> </a>
                        </li>
                    </ul>
                </div>-->
                <div class="sidebar-section">
                    <div class="section-title"></div>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a href="{$BASE_URL}administrator/dashboard/" class="sideline"> <i class="zmdi zmdi-view-dashboard md-icon pull-left"></i> <span class="title">Dashboard</span> </a>
                        </li>
                    </ul>

                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="widgets" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-widgets"></i>
                                <span class="pull-right label label-danger label-pill">Hot</span>
                                <i class="zmdi zmdi-star-circle md-icon pull-left"></i> <span class="title">Quick Report</span> </a>
                            <ul class="list-unstyled section-widgets l2">
                                <li><a class="sideline" href="#"> <span class="title">Daily Report</span> </a></li>
                                <li><a class="sideline" href="#"> <span class="title">Weekly Chart</span> </a></li>
                                <li><a class="sideline" href="#"> <span class="title">Leader Board</span> </a></li>
                            </ul>
                        </li>
                    </ul>
                </div>


                <div class="sidebar-section">
                    <div class="section-title">Basic Actions</div>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="ui" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-ui"></i>
                                <i class="zmdi zmdi-labels md-icon pull-left"></i> <span class="title">Club Settings</span> </a>
                            <ul class="list-unstyled section-ui l2">
                                <li>
                                    <a class="sideline" href="{$BASE_URL}administrator/club/create/">
                                       <span class="title"> Add Club</span>
                                    </a>
                                </li>

                                <li><a class="sideline" href="{$BASE_URL}administrator/club/list/"> <span class="title">List of Clubs</span> </a></li>
                                <li><a class="sideline" href="{$BASE_URL}administrator/club/available-leagues/"> <span class="title">Available Leagues</span> </a></li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="utilities" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-utilities"></i> <i class="zmdi zmdi-settings md-icon pull-left"></i> <span class="title">Utilities</span> </a>
                            <ul class="list-unstyled section-utilities l2">
                                <li>
                                    <a class="sideline" href="utilities-color-utilities.html"> <span class="title">Color utilities</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="utilities-border-utilities.html"> <span class="title">Border utilities</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="utilities-height-utilities.html"> <span class="title">Height utilities</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="utilities-margin-utilities.html"> <span class="title">Margin Utilities</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="utilities-other-utilities.html"> <span class="title">Other utilities</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="utilities-padding-utilities.html"> <span class="title">Padding utilities</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="utilities-position-utilities.html"> <span class="title">Position utilities</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="utilities-text-utilities.html"> <span class="title">Text utilities</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="utilities-width-utilities.html"> <span class="title">Width utilities</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="icons" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-icons"></i> <i class="zmdi zmdi-flash md-icon pull-left"></i> <span class="title">Icons</span> </a>
                            <ul class="list-unstyled section-icons l2">
                                <li>
                                    <a class="sideline" href="icons-font-awesome.html"> <span class="title">Font Awesome</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="icons-ionicons.html"> <span class="title">Ionicons</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="icons-emoji.html"> <span class="title">Emoji</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="icons-flags.html"> <span class="title">Flags</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="icons-material-design-icons.html"> <span class="title">Material Design Icons</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="icons-weather-icons.html"> <span class="title">Weather Icons</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="forms" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-forms"></i> <span class="pull-right label label-success label-pill">New</span> <i class="zmdi zmdi-folder-outline md-icon pull-left"></i> <span class="title">Forms</span> </a>
                            <ul class="list-unstyled section-forms l2">
                                <li>
                                    <a class="sideline" href="forms-basic.html"> <span class="title">Basic forms</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="forms-sample.html"> <span class="title">Sample forms</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="forms-validation.html"> <span class="title">Form Validation</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="forms-jquery-file-upload.html"> <span class="title">jQuery File Upload</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="forms-text-editor.html"> <span class="title">Text editor</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="forms-pickers.html"> <span class="title">Pickers</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="forms-nouislider.html"> <span class="title">NoUiSlider</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="tables" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-tables"></i> <i class="zmdi zmdi-format-list-numbered md-icon pull-left"></i> <span class="title">Tables</span> </a>
                            <ul class="list-unstyled section-tables l2">
                                <li>
                                    <a class="sideline" href="tables-static.html"> <span class="title">Static Tables</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="tables-datatable.html"> <span class="title">Datatable</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="tables-sortable.html"> <span class="title">Sortable</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="e-commerce" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-e-commerce"></i> <i class="zmdi zmdi-shopping-cart md-icon pull-left"></i> <span class="title">e-Commerce</span> </a>
                            <ul class="list-unstyled section-e-commerce l2">
                                <li>
                                    <a class="sideline" href="e-commerce-dashboard.html"> <span class="title">Dashboard</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="e-commerce-orders.html"> <span class="title">Orders</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="e-commerce-order.html"> <span class="title">Order</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="e-commerce-products.html"> <span class="title">Products</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="e-commerce-product.html"> <span class="title">Product</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="e-commerce-customers.html"> <span class="title">Customers</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="email" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-email"></i> <i class="zmdi zmdi-email-open md-icon pull-left"></i> <span class="title">Email</span> </a>
                            <ul class="list-unstyled section-email l2">
                                <li>
                                    <a class="sideline" href="email-inbox.html"> <span class="title">Inbox</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="email-view.html"> <span class="title">View email</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="email-compose.html"> <span class="title">Compose email</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="charts" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-charts"></i> <span class="pull-right label label-warning label-pill">info</span> <i class="zmdi zmdi-chart md-icon pull-left"></i> <span class="title">Charts</span> </a>
                            <ul class="list-unstyled section-charts l2">
                                <li>
                                    <a class="sideline" href="charts-nvd3.html"> <span class="title">NVD3</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="charts-chartist.html"> <span class="title">Chartist</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="charts-chart-js.html"> <span class="title">Chart.js</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="charts-easy-pie-chart.html"> <span class="title">Easy Pie Chart</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="charts-jquery-knob.html"> <span class="title">jQuery Knob</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="charts-gauge.html"> <span class="title">Gauges</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="pages" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-pages"></i> <i class="zmdi zmdi-file-text md-icon pull-left"></i> <span class="title">Pages</span> </a>
                            <ul class="list-unstyled section-pages l2">
                                <li>
                                    <a class="sideline" href="pages-error-page.html"> <span class="title">Error page</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="pages-empty-page.html"> <span class="title">Empty Page</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="pages-login.html"> <span class="title">Login</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="pages-register.html"> <span class="title">Register</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="pages-forgot-password.html"> <span class="title">Forgot Password</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="pages-lock-screen.html"> <span class="title">Lock Screen</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="pages-profile.html"> <span class="title">User profile</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="pages-coming-soon.html"> <span class="title">Coming soon</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="pages-under-maintenance.html"> <span class="title">Under maintenance</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="extras" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-extras"></i> <i class="zmdi zmdi-lamp md-icon pull-left"></i> <span class="title">Extras</span> </a>
                            <ul class="list-unstyled section-extras l2">
                                <li>
                                    <a class="sideline" href="extras-invoice.html"> <span class="title">Invoice</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-zoom.html"> <span class="title">Zoom</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-search-results.html"> <span class="title">Search Results</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-pricing-tables.html"> <span class="title">Pricing tables</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-wow.html"> <span class="title">Wow</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-syntax-highlighting.html"> <span class="title">Syntax highlighting</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-calendar.html"> <span class="title">Calendar</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-crop.html"> <span class="title">Image cropping</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-mousetrap.html"> <span class="title">Mousetrap</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" href="extras-typed.html"> <span class="title">Typed.js</span> </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="sidebar-section">
                    <div class="section-title">Documentation</div>
                    <ul class="l1 list-unstyled section-content">
                        <li>
                            <a class="sideline" data-id="docs" href="documentation.html"> <i class="zmdi zmdi-info-outline md-icon pull-left"></i> <span class="title">Docs</span> </a>
                        </li>
                    </ul>
                </div>
                <div class="sidebar-section">
                    <div class="section-title hidden">Menu</div>
                    <ul class="l1 list-unstyled section-content multi-level-menu">
                        <li>
                            <a class="sideline" data-id="multi-level-one" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-multi-level-one"></i> <i class="zmdi zmdi-filter-list md-icon pull-left"></i> <span class="title">Multi level menu</span> </a>
                            <ul class="l2 list-unstyled section-multi-level-one">
                                <li>
                                    <a class="sideline"> <span>Level 1 - 1</span> </a>
                                </li>
                                <li>
                                    <a class="sideline" data-id="multi-level-two" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-multi-level-two"></i> <span>Level 1 - 2</span> </a>
                                    <ul class="l3 list-unstyled section-multi-level-two">
                                        <li>
                                            <a class="sideline"> <span>Level 2 - 1</span> </a>
                                        </li>
                                        <li>
                                            <a class="sideline" data-id="multi-level-three" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-multi-level-three"></i> <span>Level 2 - 2</span> </a>
                                            <ul class="l4 list-unstyled section-multi-level-three">
                                                <li>
                                                    <a> <span>Level 3 - 1</span> </a>
                                                </li>
                                                <li>
                                                    <a class="sideline" data-id="multi-level-four" data-click="toggle-section"> <i class="pull-right fa fa-caret-down icon-multi-level-four"></i> <span>Level 3 - 2</span> </a>
                                                    <ul class="l5 list-unstyled section-multi-level-four">
                                                        <li>
                                                            <a> <span>Level 4 - 1</span> </a>
                                                        </li>
                                                        <li>
                                                            <a class="sideline"> <span>Level 4 - 2</span> </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="sidebar-labels m-t-20">
                    <h5>Tags</h5>
                    <ul class="list-unstyled">
                        <li> <i class="fa fa-dot-circle-o color-warning"></i> <span class="m-l-10">Family</span> </li>
                        <li> <i class="fa fa-dot-circle-o color-success"></i> <span class="m-l-10">Work</span> </li>
                        <li> <i class="fa fa-dot-circle-o color-danger"></i> <span class="m-l-10">Home</span> </li>
                    </ul>
                </div>
                <div class="sidebar-progress">
                    <h5>Project status</h5>
                    <div class="col-xs-8">
                        <progress class="progress-sm progress progress-warning" value="50" max="100">50%</progress>
                    </div>
                    <div class="col-xs-4"> <span class="number">50%</span> </div>
                </div>
            </div>
        </div>
    </div>
</div>