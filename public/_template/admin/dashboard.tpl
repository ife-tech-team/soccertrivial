{include file="admin/header.tpl"}
<body data-layout="empty-layout" data-palette="palette-0" data-direction="none">
    {include file="admin/top-nav.tpl"}
    {*include file="admin/side-bar.tpl"*}



<div class="container-fluid">
    <div class="row">
        <div class="sidebar-placeholder"> </div>
        {include file="admin/left-side-bar.tpl"}
        {include file="admin/right-side-bar.tpl"}

        <div class="col-xs-12 main" id="main">
            <div ng-view="" autoscroll="true">
                <div class="row m-b-40">
                    <div class="col-xs-12 col-lg-4 m-b-5">
                        <div class="text-widget-1">
                            <div class="row">
                                <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-success"></i> <i class="fa fa fa-usd fa-stack-1x color-white"></i> </span> </div>
                                <div class="col-xs-8 text-left">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="title">Sales today</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="numbers">
                                                <div> <span class="amount" count-to-currency="1123.99" value="0" duration="2">$805</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 m-b-5">
                        <div class="text-widget-1">
                            <div class="row">
                                <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-danger"></i> <i class="fa fa fa-btc fa-stack-1x color-white"></i> </span> </div>
                                <div class="col-xs-8 text-left">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="title">Revenue yesterday</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="numbers">
                                                <div> <span class="amount" count-to-currency="863.66" value="0" duration="2">$618</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 m-b-5">
                        <div class="text-widget-1">
                            <div class="row">
                                <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-warning"></i> <i class="fa fa fa-eur fa-stack-1x color-white"></i> </span> </div>
                                <div class="col-xs-8 text-left">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="title">Income last week</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="numbers">
                                                <div> <span class="amount" count-to-currency="3863.22" value="0" duration="2">$2,768</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-b-40">
                    <div class="col-xs-12 col-xl-6">
                        <div class="row m-b-20">
                            <div class="col-xs-12">
                                <div class="btn-group pull-right m-10">
                                    <button type="button" class="btn btn-warning-700 btn-rounded btn-sm">Last month</button>
                                    <button type="button" class="btn btn-warning-500 btn-rounded btn-sm">Yesterday</button>
                                    <button type="button" class="btn btn-warning-700 btn-rounded btn-sm">Today</button>
                                </div>
                                <h4> Global sales </h4>
                                <p>Last year</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="analytics-vector-map" class="vector-map" style="height:300px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-xl-6">
                        <div class="row m-b-40">
                            <div class="col-xs-12 col-xl-6">
                                <div class="col-xs-6">
                                    <div class="p-20">
                                        <h5> All sessions </h5>
                                        <p>Today</p>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="easy-pie-chart-xs">
                                        <div class="chart analytics-easy-pie-chart-1" data-percent="35"> </div>
                                        <div class="percent"> <span class="number">35</span> <span class="symbol">%</span> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-xl-6">
                                <div class="col-xs-6">
                                    <div class="p-20">
                                        <h5> New users </h5>
                                        <p>This week</p>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="easy-pie-chart-xs">
                                        <div class="chart analytics-easy-pie-chart-2" data-percent="45"> </div>
                                        <div class="percent"> <span class="number">45</span> <span class="symbol">%</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-40">
                            <div class="col-xs-12 col-xl-6">
                                <div class="col-xs-6">
                                    <div class="p-20">
                                        <h5> Mobile traffic </h5>
                                        <p>Last month</p>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="easy-pie-chart-xs">
                                        <div class="chart analytics-easy-pie-chart-3" data-percent="55"> </div>
                                        <div class="percent"> <span class="number">55</span> <span class="symbol">%</span> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-xl-6">
                                <div class="col-xs-6">
                                    <div class="p-20">
                                        <h5> Bounce rate </h5>
                                        <p>Yesterday</p>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="easy-pie-chart-xs">
                                        <div class="chart analytics-easy-pie-chart-4" data-percent="75"> </div>
                                        <div class="percent"> <span class="number">75</span> <span class="symbol">%</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-xl-6">
                                <div class="m-b-20">
                                    <div class="text-widget-7 bg-warning-700 color-white text-widget-left">
                                        <div class="row">
                                            <div class="col-xs-12"> <span class="amount" count-to-currency="2446" value="0" duration="1">$2,446</span>
                                                <div class="title">Store revenue</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-xl-6">
                                <div class="m-b-20">
                                    <div class="text-widget-7 bg-success color-white text-widget-right">
                                        <div class="row">
                                            <div class="col-xs-12"> <span class="amount" count-to-currency="12541" value="0" duration="1">$12,541</span>
                                                <div class="title">Online sales</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-xl-6">
                                <div class="m-b-20">
                                    <div class="text-widget-7 bg-danger color-white text-widget-left">
                                        <div class="row">
                                            <div class="col-xs-12"> <span class="amount" count-to-currency="5430" value="0" duration="1">$5,430</span>
                                                <div class="title">Profit</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-xl-6">
                                <div class="m-b-20">
                                    <div class="text-widget-7 bg-info color-white text-widget-right">
                                        <div class="row">
                                            <div class="col-xs-12"> <span class="amount" count-to-currency="1433" value="0" duration="1">$1,433</span>
                                                <div class="title">Ad revenue</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-b-40">
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="f-w-300">Timeline</div>
                                <p>Last hour</p>
                            </div>
                            <div class="col-xs-5">
                                <div class="dropdown pull-right m-0">
                                    <a class="btn no-bg dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-scale from-right"> <a class="dropdown-item">This week</a> <a class="dropdown-item">This month</a> <a class="dropdown-item">This year</a> <a class="dropdown-item">Today</a> </div>
                                </div>
                                <button class="btn no-bg pull-right m-0"> <i class="fa fa-cog" id="icon-824"></i> </button>
                            </div>
                        </div>
                        <div class="timeline-widget-4">
                            <div class="row bg-odd-color">
                                <div class="col-xs-12 timeline timeline-info">
                                    <div class="p-10">
                                        <p>Someone commented on your post</p>
                                        <p class="text-sm text-muted">10 minutes ago</p>
                                        <p class="text-sm m-t-10"> <span>Reply</span> <i class="m-l-5 m-r-5 fa fa-mail-reply"></i> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row bg-even-color">
                                <div class="col-xs-12 timeline timeline-danger">
                                    <div class="p-10">
                                        <p>A friend posted something on instagram</p>
                                        <p class="text-sm text-muted">30 minutes ago</p>
                                        <p class="text-sm m-t-10"> <span>Like</span> <i class="m-l-5 m-r-5 fa fa-thumbs-o-up"></i> <span>Comment</span> <i class="m-l-5 m-r-5 fa fa-comment"></i> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row bg-odd-color">
                                <div class="col-xs-12 timeline timeline-success">
                                    <div class="p-10">
                                        <p>Finished important task</p>
                                        <p class="text-sm text-muted">3 hours ago</p>
                                        <p class="text-sm m-t-10"> <span>10</span> <i class="m-l-5 m-r-5 fa fa-star-o"></i> <span>23</span> <i class="m-l-5 m-r-5 fa fa-heart-o"></i> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row bg-even-color">
                                <div class="col-xs-12 timeline timeline-success">
                                    <div class="p-10">
                                        <p>Went to mars</p>
                                        <p class="text-sm text-muted">Last month</p>
                                        <p class="text-sm m-t-10"> <span>10</span> <i class="m-l-5 m-r-5 fa fa-check"></i> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row bg-odd-color">
                                <div class="col-xs-12 timeline timeline-warning">
                                    <div class="p-10">
                                        <p>Bought a Tesla</p>
                                        <p class="text-sm text-muted">3 months ago</p>
                                        <p class="text-sm m-t-10"> <span>10</span> <i class="m-l-5 m-r-5 fa fa-twitter"></i> <span>23</span> <i class="m-l-5 m-r-5 fa fa-heart"></i> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="f-w-300">Forum members</div>
                                <p>Online status</p>
                            </div>
                            <div class="col-xs-5">
                                <div class="dropdown pull-right m-0">
                                    <a class="btn no-bg dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-scale from-right"> <a class="dropdown-item">This week</a> <a class="dropdown-item">This month</a> <a class="dropdown-item">This year</a> <a class="dropdown-item">Today</a> </div>
                                </div>
                                <button class="btn no-bg pull-right m-0"> <i class="fa fa-cog" id="icon-493"></i> </button>
                            </div>
                        </div>
                        <div class="user-widget-8">
                            <div class="row">
                                <div class="col-xs-12 bs-media">
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-success"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m1.png" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/assets/faces/m1.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Lucas smith </h5>
                                            <p>Vital Database Dude</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-info"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w1.png" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/assets/faces/w1.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Janet Abshire </h5>
                                            <p>Lead Innovation Officer</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-success"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m2.png" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/assets/faces/m2.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Lucas Koch </h5>
                                            <p>Incomparable UX Editor</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-warning"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w2.png" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/assets/faces/w2.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Gladys Schuster </h5>
                                            <p>Primary Product Pioneer</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-success"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m3.png" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/admin/assets/faces/m3.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> George Clinton </h5>
                                            <p>World Class API Genius</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="f-w-300">Browser stats</div>
                                <p>This week</p>
                            </div>
                            <div class="col-xs-5">
                                <div class="dropdown pull-right m-0">
                                    <a class="btn no-bg dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-scale from-right"> <a class="dropdown-item">This week</a> <a class="dropdown-item">This month</a> <a class="dropdown-item">This year</a> <a class="dropdown-item">Today</a> </div>
                                </div>
                                <button class="btn no-bg pull-right m-0"> <i class="fa fa-cog" id="icon-203"></i> </button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead class="hidden">
                                <tr>
                                    <th></th>
                                    <th>Browser</th>
                                    <th>Visits</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><i class="fa fa-chrome"></i> </td>
                                    <td>Google Chrome</td>
                                    <td>435</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-firefox"></i> </td>
                                    <td>Firefox</td>
                                    <td>223</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-internet-explorer"></i> </td>
                                    <td>Internet Explorer 11</td>
                                    <td>235</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-safari"></i> </td>
                                    <td>Safari</td>
                                    <td>150</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-internet-explorer"></i> </td>
                                    <td>Internet Explorer 10</td>
                                    <td>88</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-internet-explorer"></i> </td>
                                    <td>Internet Explorer 9</td>
                                    <td>35</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-opera"></i> </td>
                                    <td>Opera</td>
                                    <td>20</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row m-b-40">
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="f-w-300">Task status</div>
                                <p>3 done, 4 remaining</p>
                            </div>
                            <div class="col-xs-5">
                                <div class="dropdown pull-right m-0">
                                    <a class="btn no-bg dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-scale from-right"> <a class="dropdown-item">This week</a> <a class="dropdown-item">This month</a> <a class="dropdown-item">This year</a> <a class="dropdown-item">Today</a> </div>
                                </div>
                                <button class="btn no-bg pull-right m-0"> <i class="fa fa-cog" id="icon-494"></i> </button>
                            </div>
                        </div>
                        <div class="row m-b-10">
                            <div class="col-xs-12">
                                <p class="m-b-5 f-w-300">Product development</p>
                                <p class="text-sm">Today</p>
                                <progress class="progress-sm progress progress-warning" value="33" max="100">33%</progress>
                            </div>
                        </div>
                        <div class="row m-b-10">
                            <div class="col-xs-12">
                                <p class="bold m-b-5 f-w-300">Online shopping</p>
                                <p class="text-sm">This week</p>
                                <progress class="progress-sm progress progress-danger" value="62" max="100">62%</progress>
                            </div>
                        </div>
                        <div class="row m-b-10">
                            <div class="col-xs-12">
                                <p class="bold m-b-5 f-w-300">Space travel</p>
                                <p class="text-sm">Last month</p>
                                <progress class="progress-sm progress progress-info" value="75" max="100">75%</progress>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="f-w-300">Sales volume</div>
                                <p>Last 6 months</p>
                            </div>
                            <div class="col-xs-5">
                                <div class="dropdown pull-right m-0">
                                    <a class="btn no-bg dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-scale from-right"> <a class="dropdown-item">This week</a> <a class="dropdown-item">This month</a> <a class="dropdown-item">This year</a> <a class="dropdown-item">Today</a> </div>
                                </div>
                                <button class="btn no-bg pull-right m-0"> <i class="fa fa-cog" id="icon-880"></i> </button>
                            </div>
                        </div>
                        <canvas id="analytics-area-chart" style="width: 100%; height: 263px"></canvas>
                    </div>
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="f-w-300">Products sold</div>
                                <p>YTD</p>
                            </div>
                            <div class="col-xs-5">
                                <div class="dropdown pull-right m-0">
                                    <a class="btn no-bg dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-scale from-right"> <a class="dropdown-item">This week</a> <a class="dropdown-item">This month</a> <a class="dropdown-item">This year</a> <a class="dropdown-item">Today</a> </div>
                                </div>
                                <button class="btn no-bg pull-right m-0"> <i class="fa fa-cog" id="icon-783"></i> </button>
                            </div>
                        </div>
                        <canvas id="analytics-bar-chart" style="width: 100%; height: 264px"></canvas>
                    </div>
                </div>
                <div class="row m-b-20">
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="f-w-300">Trending topics</div>
                                <p>Last year</p>
                            </div>
                            <div class="col-xs-5">
                                <div class="dropdown pull-right m-0">
                                    <a class="btn no-bg dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-scale from-right"> <a class="dropdown-item">This week</a> <a class="dropdown-item">This month</a> <a class="dropdown-item">This year</a> <a class="dropdown-item">Today</a> </div>
                                </div>
                                <button class="btn no-bg pull-right m-0"> <i class="fa fa-cog" id="icon-689"></i> </button>
                            </div>
                        </div>
                        <div class="p-t-20">
                            <div class="series-a-success series-b-danger opacity-100" style="height:200px">
                                <div id="analytics-line-chart-1" class="ct-chart" style="height:200px"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="f-w-300">Market share</div>
                                <p>This week</p>
                            </div>
                            <div class="col-xs-5">
                                <div class="dropdown pull-right m-0">
                                    <a class="btn no-bg dropdown-toggle no-after" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-scale from-right"> <a class="dropdown-item">This week</a> <a class="dropdown-item">This month</a> <a class="dropdown-item">This year</a> <a class="dropdown-item">Today</a> </div>
                                </div>
                                <button class="btn no-bg pull-right m-0"> <i class="fa fa-cog" id="icon-322"></i> </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-lg-6">
                                <div class="series-a-danger series-b-info series-c-success series-d-warning" style="height:250px">
                                    <div id="analytics-pie-chart-4" class="ct-chart"> </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-lg-6">
                                <ul class="list-unstyled">
                                    <li> <span class="label label-success label-rounded color-white">A</span> <span>USA</span> </li>
                                    <li> <span class="label label-warning label-rounded color-white">B</span> <span>Canada</span> </li>
                                    <li> <span class="label label-danger label-rounded color-white">C</span> <span>Mexico</span> </li>
                                    <li> <span class="label label-info label-rounded color-white">D</span> <span>Argentina</span> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4">
                        <div class="row m-b-10">
                            <div class="col-xs-12">
                                <div class="text-widget-5">
                                    <div class="row">
                                        <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-white"></i> <i class="fa fa fa-heart fa-stack-1x color-danger"></i> </span> </div>
                                        <div class="col-xs-8 text-left">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="title">Likes</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="numbers">
                                                        <div> <span class="amount" count-to="4580" value="0" duration="1">4,580</span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-10">
                            <div class="col-xs-12">
                                <div class="text-widget-5">
                                    <div class="row">
                                        <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-white"></i> <i class="fa fa fa-camera fa-stack-1x color-success"></i> </span> </div>
                                        <div class="col-xs-8 text-left">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="title">Photos</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="numbers">
                                                        <div> <span class="amount" count-to="35" value="0" duration="1">35</span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-xs-12">
                                <div class="text-widget-5">
                                    <div class="row">
                                        <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-white"></i> <i class="fa fa fa-commenting fa-stack-1x color-warning"></i> </span> </div>
                                        <div class="col-xs-8 text-left">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="title">Comments</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="numbers">
                                                        <div> <span class="amount" count-to="866" value="0" duration="1">866</span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-10">
                            <div class="col-xs-12">
                                <div class="bg-twitter p-20">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="icon-widget-3">
                                                <div class="row">
                                                    <div class="col-xs-6 text-right"> <i class="fa fa-heart-o color-white"></i> </div>
                                                    <div class="col-xs-6 text-left color-white">19</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="icon-widget-3">
                                                <div class="row">
                                                    <div class="col-xs-6 text-right"> <i class="fa fa-comment-o color-white"></i> </div>
                                                    <div class="col-xs-6 text-left color-white">19</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="icon-widget-3">
                                                <div class="row">
                                                    <div class="col-xs-6 text-right"> <i class="fa fa-star-o color-white"></i> </div>
                                                    <div class="col-xs-6 text-left color-white">19</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
{include file="admin/footer.tpl"}