<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{$APP_NAME}</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icon -->
    <link rel="icon" type="image/png" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/images/icon.png">
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,700%7COpen+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/owl.carousel/dist/assets/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/magnific-popup/dist/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/youplay/css/youplay-shooter.min.css" />
    <link rel="stylesheet" type="text/css" href="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/css/custom.css" />
</head>
<body>
{include file="nav-bar.tpl"}