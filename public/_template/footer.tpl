<!-- Footer -->
<footer class="youplay-footer">
    <div class="wrapper">
        <!-- Copyright -->
        <div class="copyright">
            <div class="container">
                <strong>{$APP_NAME}</strong> &copy; 2016. All rights reserved
            </div>
        </div>
        <!-- /Copyright -->
    </div>
</footer>
<!-- /Footer -->

</section>
<!-- /Main Content -->

<!-- Search Block -->
<div class="search-block">
    <a href="index.html#!" class="search-toggle glyphicon glyphicon-remove"></a>
    <form action="search.html">
        <div class="youplay-input">
            <input type="text" name="search" placeholder="Search...">
        </div>
    </form>
</div>
<!-- /Search Block -->

<!-- jQuery -->
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/HexagonProgress/jquery.hexagonprogress.min.js"></script>
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/jarallax/dist/jarallax.min.js"></script>
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/smoothscroll-for-websites/SmoothScroll.js"></script>
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/jquery.countdown/dist/jquery.countdown.min.js"></script>
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/youplay/js/youplay.min.js"></script>
<!-- init youplay -->
<script>
    if(typeof youplay !== 'undefined') {
        youplay.init({
            parallax: true,
            navbarSmall: false,
            fadeBetweenPages: true
            // twitter and instagram php paths
            /*php: {
             twitter: './php/twitter/tweet.php',
             instagram: './php/instagram/instagram.php'
             }*/
        });
    }
</script>

</body>

</html>
