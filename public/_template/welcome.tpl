{include file="header.tpl"}
<!-- Main Content -->
<section class="content-wrap">
    <!-- Realistic Battles -->
    <section class="youplay-banner big mt-40">
        <div class="image" style="background-image: url('{$BASE_URL}{$SMARTY_VIEW_FOLDER}/assets/images/football-background.jpg');"></div>

        <div class="info">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="fs-40">How much do you <br>KNOW YOUR CLUB!!!</h2>
                            <p class="lead">Opt-in to Soccer Trivial, answer questions that pertains to your club, earn points and lead the board and you shall be rewarded greatly.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{include file="footer.tpl"}