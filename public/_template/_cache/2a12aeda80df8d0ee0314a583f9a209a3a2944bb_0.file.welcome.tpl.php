<?php /* Smarty version 3.1.24, created on 2016-05-23 09:37:11
         compiled from "public/_template/welcome.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:11786888495742c13777c1f7_93154652%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2a12aeda80df8d0ee0314a583f9a209a3a2944bb' => 
    array (
      0 => 'public/_template/welcome.tpl',
      1 => 1463992406,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11786888495742c13777c1f7_93154652',
  'variables' => 
  array (
    'BASE_URL' => 0,
    'SMARTY_VIEW_FOLDER' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5742c137795597_24457922',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5742c137795597_24457922')) {
function content_5742c137795597_24457922 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '11786888495742c13777c1f7_93154652';
echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<!-- Main Content -->
<section class="content-wrap">
    <!-- Realistic Battles -->
    <section class="youplay-banner big mt-40">
        <div class="image" style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/images/football-background.jpg');"></div>

        <div class="info">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="fs-40">How much do you <br>KNOW YOUR CLUB!!!</h2>
                            <p class="lead">Opt-in to Soccer Trivial, answer questions that pertains to your club, earn points and lead the board and you shall be rewarded greatly.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>