<?php /* Smarty version 3.1.24, created on 2016-05-23 08:49:22
         compiled from "public/_template/admin/club-create.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:21072637565742b602d18ee0_35486787%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '864e39ff08a5de77538ee7a0579812de616d22a1' => 
    array (
      0 => 'public/_template/admin/club-create.tpl',
      1 => 1463833891,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21072637565742b602d18ee0_35486787',
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5742b602d77369_36964892',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5742b602d77369_36964892')) {
function content_5742b602d77369_36964892 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '21072637565742b602d18ee0_35486787';
echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<body data-layout="empty-layout" data-palette="palette-0" data-direction="none">
<?php echo $_smarty_tpl->getSubTemplate ("admin/top-nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


<div class="container-fluid">
    <div class="row">
        <div class="sidebar-placeholder"> </div>
        <?php echo $_smarty_tpl->getSubTemplate ("admin/left-side-bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <?php echo $_smarty_tpl->getSubTemplate ("admin/right-side-bar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


        <div class="col-xs-12 main" id="main">

            <div class="row m-b-20">
                <div class="col-md-12">
                    <h4>Add a New Club</h4>
                    <p>A simple and user-friendly form validator plugin for Bootstrap. For more information click...</p>
                </div>
            </div>

            <hr class="cm-hr" />

            <div class="row m-b-40 m-t-40">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="alert alert-success alert-dismissible hidden" id="bootstrap-validator-form-success">
                        <button type="button" class="close" data-dismiss="alert"> <span>&times;</span> </button> <strong>Success!</strong> Form is valid and can be processed. </div>
                    <form id="bootstrap-validator-form" role="form" novalidate>
                        <fieldset class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" placeholder="Enter your username" required data-error="Please enter a valid username">
                            <div class="help-block with-errors"></div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Enter your email" data-error="Please enter a valid email" required>
                            <div class="help-block with-errors"></div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Password</label>
                            <input type="password" data-minlength="6" class="form-control" id="password" placeholder="Password" required data-error="Your password should have at least 6 characters">
                            <div class="help-block with-errors"></div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Confirm password</label>
                            <input type="password" class="form-control" id="confirm-password" data-match="#password" data-match-error="Please confirm your password correctly" placeholder="Password" required>
                            <div class="help-block with-errors"></div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="c-input c-radio">
                                <input id="radio1" name="radio" type="radio" required> <span class="c-indicator c-indicator-success"></span> Option A </label>
                            <label class="c-input c-radio">
                                <input id="radio2" name="radio" type="radio" required> <span class="c-indicator c-indicator-success"></span> Option B </label>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="c-input c-checkbox">
                                <input type="checkbox" id="terms" data-error="You should really check this" required> <span class="c-indicator c-indicator-warning"></span> Check this </label>
                            <div class="help-block with-errors"></div>
                        </fieldset>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>