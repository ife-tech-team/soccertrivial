<?php /* Smarty version 3.1.24, created on 2016-05-21 16:52:20
         compiled from "public/_template/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1394236502574084342aa326_84238025%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '571694cf505371b6ff1854a12be619e65e28d003' => 
    array (
      0 => 'public/_template/footer.tpl',
      1 => 1463845937,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1394236502574084342aa326_84238025',
  'variables' => 
  array (
    'APP_NAME' => 0,
    'BASE_URL' => 0,
    'SMARTY_VIEW_FOLDER' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_574084342ba866_69513985',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_574084342ba866_69513985')) {
function content_574084342ba866_69513985 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1394236502574084342aa326_84238025';
?>
<!-- Footer -->
<footer class="youplay-footer">
    <div class="wrapper">
        <!-- Copyright -->
        <div class="copyright">
            <div class="container">
                <strong><?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
</strong> &copy; 2016. All rights reserved
            </div>
        </div>
        <!-- /Copyright -->
    </div>
</footer>
<!-- /Footer -->

</section>
<!-- /Main Content -->

<!-- Search Block -->
<div class="search-block">
    <a href="index.html#!" class="search-toggle glyphicon glyphicon-remove"></a>
    <form action="search.html">
        <div class="youplay-input">
            <input type="text" name="search" placeholder="Search...">
        </div>
    </form>
</div>
<!-- /Search Block -->

<!-- jQuery -->
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/bower_components/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/bower_components/HexagonProgress/jquery.hexagonprogress.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/bower_components/jarallax/dist/jarallax.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/bower_components/smoothscroll-for-websites/SmoothScroll.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/bower_components/owl.carousel/dist/owl.carousel.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/bower_components/jquery.countdown/dist/jquery.countdown.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/youplay/js/youplay.min.js"><?php echo '</script'; ?>
>
<!-- init youplay -->
<?php echo '<script'; ?>
>
    if(typeof youplay !== 'undefined') {
        youplay.init({
            parallax: true,
            navbarSmall: false,
            fadeBetweenPages: true
            // twitter and instagram php paths
            /*php: {
             twitter: './php/twitter/tweet.php',
             instagram: './php/instagram/instagram.php'
             }*/
        });
    }
<?php echo '</script'; ?>
>

</body>

</html>
<?php }
}
?>