<?php /* Smarty version 3.1.24, created on 2016-05-23 09:37:11
         compiled from "public/_template/nav-bar.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1038793205742c1377bc124_46878684%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f7d0d9ab119e31277db327e7e0d7118fd28882e1' => 
    array (
      0 => 'public/_template/nav-bar.tpl',
      1 => 1463992406,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1038793205742c1377bc124_46878684',
  'variables' => 
  array (
    'BASE_URL' => 0,
    'SMARTY_VIEW_FOLDER' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5742c1377cd990_67053904',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5742c1377cd990_67053904')) {
function content_5742c1377cd990_67053904 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1038793205742c1377bc124_46878684';
?>
<!-- Navbar -->
<nav class="navbar-youplay navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="off-canvas" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
">
                <img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/images/logo.png" alt="">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#"><!-- <?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
info/how-to-participate/ -->
                        Quick Info
                        <span class="label">How to Participate</span>
                    </a>
                </li>
                <li>
                    <a href="#"><!-- <?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
leader-board/-->
                        Leader Board
                        <span class="label">List of Winners</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Community
                        <span class="label">Forums</span>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-hover dropdown-cart">
                    <a href="javascript:void(0)" class="dropdown-toggle login-string" data-toggle="dropdown" role="button" aria-expanded="true">
                        <i class="fa fa-lock"></i> &nbsp;&nbsp;Sign In / Register
                    </a>
                    <div class="dropdown-menu" style="width: 300px;">
                        <div class="block-content col-md-12 mnb-10">
                            <div class="lwa lwa-default">
                                <form class="block-content" action="#" method="post">
                                    <p>Username:</p>
                                    <div class="youplay-input">
                                        <input type="text" name="log">
                                    </div>

                                    <p>Password:</p>
                                    <div class="youplay-input">
                                        <input type="password" name="pwd">
                                    </div>

                                    <div class="youplay-checkbox mb-15 ml-5">
                                        <input type="checkbox" name="rememberme" value="forever" id="rememberme" tabindex="103">
                                        <label for="rememberme">Remember Me</label>
                                    </div>

                                    <div class="m-r-10 m-b-10"></div>

                                    <button class="btn btn-sm ml-0 mr-0 btn-block" name="wp-submit" id="lwa_wp-submit" tabindex="100">Log In</button>

                                    <br>
                                    <p><small>Don't have an account yet...? <a href="index.html#">Register</small></a>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- /Navbar --><?php }
}
?>