<?php /* Smarty version 3.1.24, created on 2016-05-23 08:49:15
         compiled from "public/_template/admin/right-side-bar.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:10769430135742b5fb8b4a79_63613328%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5d53d67a592e89fa739450ec3ef7505400775ce6' => 
    array (
      0 => 'public/_template/admin/right-side-bar.tpl',
      1 => 1463818115,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10769430135742b5fb8b4a79_63613328',
  'variables' => 
  array (
    'BASE_URL' => 0,
    'SMARTY_VIEW_FOLDER' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5742b5fb9002c6_58652373',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5742b5fb9002c6_58652373')) {
function content_5742b5fb9002c6_58652373 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '10769430135742b5fb8b4a79_63613328';
?>
<div class="right-sidebar-outer">
    <div class="right-sidebar-inner">
        <div class="right-sidebar">
            <div class="bs-nav-tabs nav-tabs-warning justified">
                <ul class="nav nav-tabs nav-animated-border-from-center">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" data-target="#rtab-left">Activities</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" data-target="#rtab-center">Users</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" data-target="#rtab-right">Stats</a> </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="rtab-left">
                        <h5>Today</h5>
                        <div class="timeline-widget-4">
                            <div class="row bg-odd-color">
                                <div class="col-xs-12 timeline timeline-info">
                                    <div class="p-10">
                                        <p>Someone commented on your post</p>
                                        <p class="text-sm text-muted">10 minutes ago</p>
                                        <p class="text-sm m-t-10"> <span>Reply</span> <i class="m-l-5 m-r-5 fa fa-mail-reply"></i> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row bg-even-color">
                                <div class="col-xs-12 timeline timeline-danger">
                                    <div class="p-10">
                                        <p>A friend posted something on instagram</p>
                                        <p class="text-sm text-muted">30 minutes ago</p>
                                        <p class="text-sm m-t-10"> <span>Like</span> <i class="m-l-5 m-r-5 fa fa-thumbs-o-up"></i> <span>Comment</span> <i class="m-l-5 m-r-5 fa fa-comment"></i> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row bg-odd-color">
                                <div class="col-xs-12 timeline timeline-success">
                                    <div class="p-10">
                                        <p>Finished important task</p>
                                        <p class="text-sm text-muted">3 hours ago</p>
                                        <p class="text-sm m-t-10"> <span>10</span> <i class="m-l-5 m-r-5 fa fa-star-o"></i> <span>23</span> <i class="m-l-5 m-r-5 fa fa-heart-o"></i> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row bg-even-color">
                                <div class="col-xs-12 timeline timeline-success">
                                    <div class="p-10">
                                        <p>Went to mars</p>
                                        <p class="text-sm text-muted">Last month</p>
                                        <p class="text-sm m-t-10"> <span>10</span> <i class="m-l-5 m-r-5 fa fa-check"></i> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row bg-odd-color">
                                <div class="col-xs-12 timeline timeline-warning">
                                    <div class="p-10">
                                        <p>Bought a Tesla</p>
                                        <p class="text-sm text-muted">3 months ago</p>
                                        <p class="text-sm m-t-10"> <span>10</span> <i class="m-l-5 m-r-5 fa fa-twitter"></i> <span>23</span> <i class="m-l-5 m-r-5 fa fa-heart"></i> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5>Stream</h5>
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="list-group activities">
                                    <li class="list-group-item"> <span class="text-xs pull-right">4 minutes ago</span> <span class="label label-success">charge</span>
                                        <div class="clearfix"></div> <span class="capitalize">Michael Smith</span> <span class="text-bold">upgraded to</span> the $50 plan</li>
                                    <li class="list-group-item"> <span class="text-xs pull-right">34 minutes ago</span> <span class="label label-primary">charge</span>
                                        <div class="clearfix"></div> <span class="capitalize">Jane Perez</span> <span class="text-bold">canceled</span> the $25 plan</li>
                                    <li class="list-group-item"> <span class="text-xs pull-right">an hour ago</span> <span class="label label-primary">upgrade</span>
                                        <div class="clearfix"></div> <span class="capitalize">George Clinton from Facebook</span> <span class="text-bold">upgraded to</span> the $25 plan</li>
                                    <li class="list-group-item"> <span class="text-xs pull-right">2 hours ago</span> <span class="label label-warning">cancelled</span>
                                        <div class="clearfix"></div> <span class="capitalize">Someone from Google</span> <span class="text-bold">canceled</span> the $25 plan</li>
                                    <li class="list-group-item"> <span class="text-xs pull-right">2 hours ago</span> <span class="label label-success">upgrade</span>
                                        <div class="clearfix"></div> <span class="capitalize">Lucas Smith</span> <span class="text-bold">upgraded to</span> the $25 plan</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="rtab-center">
                        <h5>Online</h5>
                        <div class="user-widget-8">
                            <div class="row">
                                <div class="col-xs-12 bs-media">
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-success"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m1.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/m1.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Lucas smith </h5>
                                            <p>Vital Database Dude</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-info"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w1.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/w1.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Janet Abshire </h5>
                                            <p>Lead Innovation Officer</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-success"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m2.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/m2.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Lucas Koch </h5>
                                            <p>Incomparable UX Editor</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-warning"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w2.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/w2.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Gladys Schuster </h5>
                                            <p>Primary Product Pioneer</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-success"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m3.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/m3.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> George Clinton </h5>
                                            <p>World Class API Genius</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5>Offline</h5>
                        <div class="user-widget-8">
                            <div class="row">
                                <div class="col-xs-12 bs-media">
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-danger"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w10.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/w10.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Kirsten Perez </h5>
                                            <p>Project Management Researcher</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-danger"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m10.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/m10.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Jerome Lynch </h5>
                                            <p>Android Engineer</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-success"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w9.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/w9.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Hannah Cook </h5>
                                            <p>Wise Email Captain</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-warning"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m9.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/m9.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Melvin Hicks </h5>
                                            <p>Primary PHP Monkey</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-danger"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w8.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/w8.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Jackie Perkins </h5>
                                            <p>Executive PR Evangelist</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-info"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m8.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/m8.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Michael Jenkins </h5>
                                            <p>Ruby On Rails Developer</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-success"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w7.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/w7.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Monica White </h5>
                                            <p>iOS Strategist</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-danger"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m7.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/m7.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Eric Simpson </h5>
                                            <p>Innovation Pioneer</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-warning"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/w6.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/w6.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> Clorinda Murphy </h5>
                                            <p>Social Media Writer</p>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <a class="media-left"> <i class="fa fa-circle icon color-primary"></i> <img class="media-object img-circle h-40 w-40" alt="/assets/faces/m6.png" src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/admin/assets/faces/m6.png"> </a>
                                        <div class="media-body">
                                            <h5 class="media-heading"> James Smith </h5>
                                            <p>Project Management Champ</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="rtab-right">
                        <h5>Sales</h5>
                        <div class="m-b-10">
                            <div class="text-widget-1">
                                <div class="row">
                                    <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-warning"></i> <i class="fa fa fa-usd fa-stack-1x color-white"></i> </span> </div>
                                    <div class="col-xs-8 text-left">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="title">Today</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="numbers">
                                                    <div> <span class="amount" count-to-currency="1123.99" value="0" duration="2">$1,123</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-b-10">
                            <div class="text-widget-1">
                                <div class="row">
                                    <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-success"></i> <i class="fa fa fa-eur fa-stack-1x color-white"></i> </span> </div>
                                    <div class="col-xs-8 text-left">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="title">Yesterday</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="numbers">
                                                    <div> <span class="amount" count-to-currency="1844" value="0" duration="2">$1,844</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-b-10">
                            <div class="text-widget-1">
                                <div class="row">
                                    <div class="col-xs-4"> <span class="fa-stack fa-stack-2x pull-left"> <i class="fa fa-circle fa-stack-2x color-danger"></i> <i class="fa fa fa-btc fa-stack-1x color-white"></i> </span> </div>
                                    <div class="col-xs-8 text-left">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="title">This week</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="numbers">
                                                    <div> <span class="amount" count-to-currency="7485" value="0" duration="2">$7,485</span> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5>Twitter</h5>
                        <div class="m-b-10">
                            <div class="icon-widget-3 bg-twitter">
                                <div class="row icon">
                                    <div class="col-xs-12 text-center"> <i class="color-white"></i> </div>
                                </div>
                                <div class="row icons">
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-twitter color-white"></i> <span class="color-white">90</span> </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-comment-o color-white"></i> <span class="color-white">22</span> </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-star-o color-white"></i> <span class="color-white">33</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5>Facebook</h5>
                        <div class="m-b-10">
                            <div class="icon-widget-3 bg-facebook">
                                <div class="row icon">
                                    <div class="col-xs-12 text-center"> <i class="color-white"></i> </div>
                                </div>
                                <div class="row icons">
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-thumbs-o-up color-white"></i> <span class="color-white">45</span> </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-comment-o color-white"></i> <span class="color-white">51</span> </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-star-o color-white"></i> <span class="color-white">13</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5>Google</h5>
                        <div class="m-b-10">
                            <div class="icon-widget-3 bg-google">
                                <div class="row icon">
                                    <div class="col-xs-12 text-center"> <i class="color-white"></i> </div>
                                </div>
                                <div class="row icons">
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-heart-o color-white"></i> <span class="color-white">8</span> </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-comment-o color-white"></i> <span class="color-white">13</span> </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-lg-4 text-center">
                                        <div class="amount"> <i class="fa fa-star-o color-white"></i> <span class="color-white">44</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5>Poll results</h5>
                        <div class="m-b-10 p-20">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p> <span class="pull-right">24%</span> <span>Spain</span> </p>
                                    <progress class="progress-sm progress progress-warning" value="24" max="100">24%</progress>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p> <span class="pull-right">52%</span> <span>USA</span> </p>
                                    <progress class="progress-sm progress progress-danger" value="52" max="100">52%</progress>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p> <span class="pull-right">68%</span> <span>Australia</span> </p>
                                    <progress class="progress-sm progress progress-info" value="68" max="100">68%</progress>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p> <span class="pull-right">45%</span> <span>Sweden</span> </p>
                                    <progress class="progress-sm progress progress-success" value="45" max="100">45%</progress>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
?>