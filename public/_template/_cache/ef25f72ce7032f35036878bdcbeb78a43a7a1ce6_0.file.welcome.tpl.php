<?php /* Smarty version 3.1.24, created on 2016-05-21 18:56:08
         compiled from "public/_template/welcome.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:9474250545740a138cca869_05235643%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ef25f72ce7032f35036878bdcbeb78a43a7a1ce6' => 
    array (
      0 => 'public/_template/welcome.tpl',
      1 => 1463853365,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9474250545740a138cca869_05235643',
  'variables' => 
  array (
    'BASE_URL' => 0,
    'SMARTY_VIEW_FOLDER' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.24',
  'unifunc' => 'content_5740a138d15643_21612867',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5740a138d15643_21612867')) {
function content_5740a138d15643_21612867 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '9474250545740a138cca869_05235643';
echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<!-- Main Content -->
<section class="content-wrap">
    <!-- Realistic Battles -->
    <section class="youplay-banner big mt-40">
        <div class="image" style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;
echo $_smarty_tpl->tpl_vars['SMARTY_VIEW_FOLDER']->value;?>
/assets/images/football-background.jpg');"></div>

        <div class="info">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="fs-40">How much do you <br>KNOW YOUR CLUB!!!</h2>
                            <p class="lead">Opt-in to Soccer Trivial, answer questions that pertains to your club, earn points and lead the board and you shall be rewarded greatly.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>