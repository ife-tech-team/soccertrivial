<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kannel_Processor extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->library("rabbitmq");
    }


    public function index(){
        echo 'Access Denied.....';
    }

    public function init(){
        $this->load->model('request_model','requests');
         $callback = function($msg) {
            $message = $msg->body;
            $msg_body = json_decode($message,true);
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            $this->requests->add($msg_body);
             //@Todo handle so many process here okay.....
             $this->requests->decipher($msg_body);
         };
         $this->rabbitmq->consume_from_queue($this->config->item("rmq-queue")["kannel-request"],$callback);
    }



    public function registration(){
        $this->load->model("user_model","user");

        $callback = function($msg) {
            $message = $msg->body;
            $msg_body = json_decode($message,true);
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

            $check = $this->user->check(($msg_body["msisdn"]));
            $msisdn= $this->user->msisdn_sanitizer($msg_body["msisdn"]);
            if($check){
                //user already exist okay....account-exist
                $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                    array("msidn"=>$msisdn,"msg"=>str_replace("{USERNAME}",$check["username"],
                        $this->config->item("rmq-msg")["acc-exist"])));
            }else{
                //add user okay....

                $data = array("msisdn"=>$msisdn,"username"=>$msg_body["username"],"gender"=>$msg_body["gender"],
                    "club_id"=>$msg_body["club_id"],"level"=>1,"status"=>1,"points"=>0, "answered_questions" => array() ,"date_added"=>time());
                $this->user->add($data);

                //send message to user
                $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                    array("msidn"=>$msg_body["msisdn"],"msg"=>str_replace("{USERNAME}",$msg_body["username"],
                        str_replace("{CLUB_NAME}",$msg_body["club_name"],$this->config->item("rmq-msg")["reg-success"]))));

            }
        };
        $this->rabbitmq->consume_from_queue($this->config->item("rmq-queue")["reg-queue"],$callback);
    }



}
