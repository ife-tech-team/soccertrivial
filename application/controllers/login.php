<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index(){
        $data = array();

        if(!empty($_POST)){
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $error = array();

            if(empty($username)) $error[] = 'Invalid Username';
            if(empty($password)) $error[] = 'Invalid Password';

            if(empty($error)){
                $this->load->model('user_model','user');
                $data = $this->user->check(array('username'=>$username,'passkey'=>md5($password)));

                if(!empty($data)){

                    if($data['privilege'] != 1){
                        $data['error'] = array('Sorry, your account has been blocked by administrator.');
                    }else {
                        $this->session->set_userdata('admin-user', $data);
                        redirect('dashboard/');
                    }
                }else
                    $data['error'] = array('Invalid Login Credentials');
            }else
                $data['error'] = $error;
        }
        $this->smarty->view('login.tpl', $data );
    }
}
