<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Club extends REST_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('club_model','club');
    }


    public function load_get(){
        $search = $this->get("search");
        $data = $this->club->get(array("keywords"=>array('$in'=>array($search))));

        //array("fields"=>array("abbrev","name"))
        $this->response_ok($data);
    }


    public function create_post(){
        //_id, name, abbrev, classification, keywords
        //if(!empty($))
        $name = $this->input->post('name');
        $abbrev = $this->input->post('abbrev');
        $league = $this->input->post('league');
        $keywords = $this->input->post('keywords'); //comma separated strings

        $error = array();

        if(empty($name))  $error[] = 'Invalid Club Name';
        if(empty($abbrev)) $error[] = 'Provide Club Abbreviation';
        if(empty($league)) $error[] = 'League Club belongs too';
        if(empty($keywords)) $error[] = 'Provide Keywords to match Club Search';

        if(empty($error)){
            //add club to mongo
            $status = $this->club->add(array('name'=>$name,'abbrev'=>$abbrev,'league'=>$league,'keywords'=>explode(',',$keywords)));
            if($status){
                $this->response_ok($status);
            }else{
                $this->response_bad("Error adding Club, Seems ".$name." already exist.");
            }
        }else{
            $this->response_bad($error);
        }
    }

}
