<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Kannel extends REST_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->library("rabbitmq");
    }


    public function index_get(){
        echo 'Access Denied.....';
    }


    public function request_get(){
        $msisdn = $this->get('msisdn');
        $shortcode = $this->get('shortcode');
        $time = $this->get('time');
        $message = $this->get('message');
        $test = $this->get('test');
        

        $error = array();

        if(empty($msisdn)) $error[] = 'Invalid MSISDN';
        if(empty($shortcode)) $error[] = 'Uknown Shortcode';
        if(empty($message)) $error[] = 'No Message Sent';



        if(empty($error)){
            //successful validation...
            $data = array('msisdn'=>$msisdn,'shortcode'=>$shortcode,'time'=>time(),'message'=>$message);

            $status = $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["kannel-request"],$data);
            if(!empty($test)) $this->response_ok($status);

        }else{
            if(!empty($test)) $this->response_bad("In-complete Parameter",$error);
        }
    }



}
