<?php


defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Question extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('question_model','questions');

    }

    public function question_get(){
        $search_query = $this->get('search');
        $data = $this->questions->get(array("keywords"=>array('$in'=>array($search_query))));
        if ($data){
            $this->response_ok($data);
        }
        else {
            $this->response_bad('Error getting question');
        }
    }


    public function create_post()
    {
        //_id, name, abbrev, classification, keywords
        //if(!empty($))
        $question = $this->input->post('question');
        $club = $this->input->post('club');
        $level = $this->input->post('level');
        $options = $this->input->post('options');
        $answer = $this->input->post("answer");
        //comma separated strings

        $error = array();

        if (empty($question)) $error[] = 'Invalid Question';
        if (empty($club)) $error[] = 'Provide Club for the question';
        if (empty($level)) $error[] = 'Choose a question level';
        if (empty($options)) $error[] = 'Provide options to the question';
        if (!is_numeric($answer)) $error[] = 'Provide an answer to the question';

        if (empty($error)) {
            //add question to mongo
            $status = $this->questions->add(array('question' => $question, 'club' => $club, 'level' => $level, 'options' => explode(',', $options),
                                                    'answer'=> $answer));
            if ($status) {
                $this->response_ok($status);
            } else {
                $this->response_bad("Error adding Question, Seems " . $question . " already exist.");
            }
        } else {
            $this->response_bad($error);
        }

    }

}