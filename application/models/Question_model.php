<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        $mongo_option = array('model' => $this->config->item('mongo_models')['questions'],
            'indexes' => $this->config->item('mongo_indexes')['questions']);
        $this->load->library("predis");
        $this->mongoci->init($mongo_option);

    }


    public function check($level)
    {
        return $this->predis->get_set(array("level" => $level));
    }


    public function add($data)
    {
        $state = $this->mongoci->_save($data);
        if (empty($state) || isset($state->error)) {
            return false;
        }
        else{
            //add to redis okay...
            $redis_data = $state;
            unset($redis_data["options"]);
            $this->predis->add_set(array("key"=>array("question"=>$state["_id"]),"data"=>$redis_data));
            return $state;
        }
    }


    public function get($condition,$field=array()){
        $data = $this->mongoci->_get($condition,$field);
        if(empty($data) || isset($data->error)){
            return false;
        }else{
            return $data;
        }
    }

    public function get_all($condition=array(),$option=array()){
        $data = $this->mongoci->_get_bulk($condition,$option);
        if(empty($data) || isset($data->error)){
            return false;
        }else{
            return $data;
        }
    }

    public function update($data,$condition)
    {
        $state = $this->mongoci->_update($data, $condition);
        if (empty($state) || isset($state->error)) {
            return false;
        } else {
            $redis_data = $state;
            unset($redis_data["options"]);
            $this->predis->add_set(array("key" => array("question" => $state["question"], "options"=>$state["options"]), "data" => $state));
            return $state["_id"];
        }
    }

    public function next($msisdn){
        $this->load->model("question_model");
        $user_profile = $this->user_model->check($msisdn);
        $answered_questions = json_decode($user_profile['answered_questions'], true);
        $question = $this->question_model->get(array('level' => $user_profile['level'],
            '_id' => array('$nin' => $answered_questions, 'club' => $user_profile['club_id'])),
            array('question', 'options', '_id'));

        $question_string = $this->config->item("rmq-msg")["question-msg"];
        $question_string = str_replace("{QUESTION_COUNTER}", count($answered_questions) + 1, $question_string);
        $question_string = str_replace("{LEVEL_COUNTER}", $user_profile['level'], $question_string);
        $question_string = str_replace("{QUESTION}", $question['question'], $question_string);

        $options = "";
        for ($i = 0; $i < count($question['options']); $i++) {
            $options .= ";" . ($i + 1) . " FOR '" . $question['options'][$i] . "' ";
        }

        $question_string = str_replace("{OPTIONS_AND_VALUE}", $options, $question_string);
        $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
            array("msisdn" => $msisdn, "msg" => $question_string));


        $this->question_model->update(array($user_profile['answered_questions']=>$question['_id']));
    }

    public function answer_question($keyword,$profile, $msisdn)
    {
        //@Todo: manage answer to questions here...
        $this->load->model('question_model');
        $current_question = count($profile['answered_questions']) - 1;
        $user_profile = $this->user_model->check($msisdn);
        //Get the current question
        $question_index = count(($user_profile['answered_questions'] - 1));
        $answer = $this->question_model->get(array('_id' => array('$in' => $user_profile['answered_questions']), array('$slice' => $question_index, 'answer')));
        if ($keyword == $answer) {
            $this->user_model->update($data = array($user_profile['points'] => ($user_profile['points'] + 1)));
            $this->user_model->move_to_next_level($msisdn);
            $this->question_model->next($msisdn);

        } elseif (!$keyword = $answer) {

            $this->user_model->update($data = array($user_profile['points'] => ($user_profile['points'])));
            $this->question_model->answer_question($keyword, $user_profile);
            $this->question_model->next($msisdn);

        }

    }







}