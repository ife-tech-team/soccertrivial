<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_model extends CI_Model {

    function __construct(){
        parent::__construct();
        $mongo_option = array('model'=>$this->config->item('mongo_models')['requests']);
        $this->mongoci->init($mongo_option);
        $this->load->library("rabbitmq");

    }

    public function test($data,$condition){
        $status = $this->mongoci->_update_bulk($data,$condition);
        return $status;
    }

    public function add($data){
        //msisdn, message, time and shortcode.....
        $this->mongoci->_save($data);
    }


    public function decipher($request)
    {

        $msisdn = $request['msisdn'];
        $message = $request['message'];

        if (!empty($message)) {
            $msg_token = explode(",", $message);

            if (is_array($msg_token) && count($msg_token) > 1) {
                $this->load->model("club_model");
                $club_info = $this->club_model->get(array("keywords" => array('$in' => array(trim($msg_token[1])))));
                $error = array();

                if (empty($msg_token[0])) $error[] = "Invalid Username";
                if (empty($msg_token[1])) $error[] = "Club Not Provided";
                if (empty($club_info)) $error[] = "Invalid Club Details";
                if (empty($msg_token[2])) $error[] = "Provide your Gender";

                if (empty($error)) {
                    $data = array("msisdn" => $msisdn, "username" => $msg_token[0], "club" => $club_info["abbrev"], "gender" => $msg_token[2],
                        "club_id" => $club_info["_id"], "club_name" => $club_info["name"]);
                    $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["reg-queue"], $data);
                } else {
                    $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                        array("msidn" => $msisdn, "msg" => str_replace("{REASON}", $error[0], $this->config->item("rmq-msg")["reg-error"])));
                }
            } else {
                //it could be something else okay....
                $this->load->model('user_model');
                $keyword = strtolower($message);
                $user_profile = $this->user_model->check($msisdn);

                if (empty($user_profile)) { //user doesn't exist...
                    //@Todo: perform all tasks that doens't require user account creation

                    switch ($keyword) {
                        case "help":
                            //@Todo: Send Help Message to Generic Message Queue
                            $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                                array("msisdn" => $msisdn, "msg" => $this->config->item("rmq-msg")["help-msg"])
                            );

                            break;


                        case "leaderboard":
                            //@Todo: Send top [configred_number_of_players] scored players...
                            $this->load->model('club_model');

                            $score = $this->user_model->get(array('points' => array('$gt' => 50)),
                                                            $fields = array('points', 'level', 'username', 'club_id'));
                            $user_club = $this->club_model->get(array('_id'=>$score['club_id']), array('name'));
                            $leaderboard_string = $this->config->item("rmq-msg")["lead_msg"];
                            $leaderboard_string = str_replace("{USERNAME}", $score["username"], $leaderboard_string);
                            $leaderboard_string = str_replace("{POINTS}", $score['points'], $leaderboard_string);
                            $leaderboard_string = str_replace("{CLUB}", $user_club['name'], $leaderboard_string);
                            $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                               array("msidn" => $msisdn, 'msg'=> $leaderboard_string));
                            break;

                        default:
                            //@Todo: Send Invalid Keyword Message to user.
                            $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                                array("msidn" => $msisdn, "msg" => $this->config->item("rmq-msg")["reg-msg"]));
                                break;
                    }
                } else {
                    //@Todo: perform all taks that needs existing account

                    if (is_numeric($keyword)) { //trying to respond to a question
                        //@Todo: manage answer to questions here...
                        $this->load->model('question_model');


                        $answer = $this->question_model->get(array('_id'=>array('$in'=>$user_profile['answered_questions']), array('$slice'=> $question_index,'answer')));
                        $this->question_model->answer_question($keyword, $user_profile);

 }

                        else {

                            switch ($keyword) {
                                case "score":
                                case "point":
                                    //@Todo: get user current point
                                    $score_string = $this->config->item("rmq-msg")["score_msg"];
                                    $score_string = str_replace("{USERNAME}",$user_profile['name'], $score_string);
                                    $score_string = str_replace("{SCORE}",$user_profile['points'],$score_string);
                                    $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                                        array("msisdn" => $msisdn, "msg" => $score_string));
                                    break;

                                case "begin":
                                    $this->load->model("question_model");
                                    $answered_questions = json_decode($user_profile['answered_questions'], true);
                                    $question = $this->question_model->get(array('level' => $user_profile['level'],
                                        '_id' => array('$nin' => $answered_questions, 'club' => $user_profile['club_id'])),
                                        array('question', 'options', '_id'));

                                    $question_string = $this->config->item("rmq-msg")["question-msg"];
                                    $question_string = str_replace("{QUESTION_COUNTER}", count($answered_questions) + 1, $question_string);
                                    $question_string = str_replace("{LEVEL_COUNTER}", $user_profile['level'], $question_string);
                                    $question_string = str_replace("{QUESTION}", $question['question'], $question_string);

                                    $options = "";
                                    for ($i = 0; $i < count($question['options']); $i++) {
                                        $options .= ";" . ($i + 1) . " FOR '" . $question['options'][$i] . "' ";
                                    }

                                    $question_string = str_replace("{OPTIONS_AND_VALUE}", $options, $question_string);
                                    $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                                        array("msisdn" => $msisdn, "msg" => $question_string));


                                    $this->question_model->update(array($user_profile['answered_questions']=>$question['_id']));

                                    break;

                                case "quit":
                                    //@Todo: End the current user's session.
                                    $this->user_profile->update(array($user_profile['status']=> 0));
                                    $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                                        array("msisdn" => $msisdn, 'msg' => $this->config->item("rmq-msg")["quit-msg"]));

                                    break;

                                case "help":
                                    //@Todo: Send Help Message to Generic Message Queue
                                    $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                                        array("msisdn" => $msisdn, "msg" => $this->config->item("rmq-msg")["help-msg"])
                                    );

                                    break;

                                case "leaderboard":
                                    //@Todo: Send top [configred_number_of_players] scored players...
                                    $this->load->model('club_model');

                                    $score = $this->user_model->get(array('points' => array('$gt' => 50)),
                                        $fields = array('points', 'level', 'username', 'club_id'));
                                    $user_club = $this->club_model->get(array('_id'=>$score['club_id']), array('name'));
                                    $leaderboard_string = $this->config->item("rmq-msg")["lead_msg"];
                                    $leaderboard_string = str_replace("{USERNAME}", $score["username"], $leaderboard_string);
                                    $leaderboard_string = str_replace("{POINTS}", $score['points'], $leaderboard_string);
                                    $leaderboard_string = str_replace("{CLUB}", $user_club['name'], $leaderboard_string);
                                    $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                                        array("msidn" => $msisdn, 'msg'=> $leaderboard_string));
                                    break;

                                default:
                                    //@Todo: Send Invalid Keyword Message to user.
                                    $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"],
                                        array("msidn" => $msisdn, "msg" => $this->config->item("rmq-msg")["invalid-ms"]));

                                    break;
                            }
                        }
                    }
                }
            }




        else{
                $this->rabbitmq->push_to_queue($this->config->item("rmq-queue")["msg-queue"], array("msidn" => $msisdn, "msg" => $this->config->item("rmq-msg")["invalid-msg"]));
            }


    }


}
