<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Club_Model extends CI_Model {


    function __construct(){
        parent::__construct();
        $mongo_option = array('model'=>$this->config->item('mongo_models')['clubs'],
                              'indexes'=>$this->config->item('mongo_indexes')['clubs']);
        $this->load->library("predis");
        $this->mongoci->init($mongo_option);
    }



    public function check($_id){
        return $this->predis->get_set(array("club-info"=>$_id));
    }


    public function add($data){
        $state = $this->mongoci->_save($data);
        if(empty($state) || isset($state->error)){
            return false;
        }else{
            //add to redis okay...
            $redis_data = $state;
            unset($redis_data["keywords"]);
            $this->predis->add_set(array("key"=>array("club-info"=>$state["_id"]),"data"=>$redis_data));
            return $state;
        }
    }

    public function update($data,$condition){
        $state = $this->mongoci->_update($data,$condition);
        if(empty($state) || isset($state->error)){
            return false;
        }else{
            $redis_data = $state;
            unset($redis_data["keywords"]);
            $this->predis->add_set(array("key"=>array("club-info"=>$state["_id"]),"data"=>$state));
            return $state["_id"];
        }
    }


    public function get($condition,$field=array()){
        $data = $this->mongoci->_get($condition,$field);
        if(empty($data) || isset($data->error)){
            return false;
        }else{
            return $data;
        }
    }


    public function get_all($condition=array(),$option=array()){
        $data = $this->mongoci->_get_bulk($condition,$option);
        if(empty($data) || isset($data->error)){
            return false;
        }else{
            return $data;
        }
    }


}
