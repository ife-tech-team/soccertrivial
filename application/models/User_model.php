<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {

	function __construct(){
		parent::__construct();

		$mongo_option = array('model'=>$this->config->item('mongo_models')['users'],'indexes'=>$this->config->item('mongo_indexes')['users']);
		$this->load->library("predis");
		$this->mongoci->init($mongo_option);
	}


	public function check($msisdn){
		return $this->predis->get_set(array("user-profile"=>$msisdn));
	}


	public function add($data){
		$state = $this->mongoci->_save($data);
		if(empty($state) || isset($state->error)){
			return false;
		}else{
			//add to redis okay...
			if(isset($state['answered_questions'])) $state['answered_questions'] = json_encode($state['answered_questions']);
			$this->predis->add_set(array("key"=>array("user-profile"=>$state["msisdn"]),"data"=>$state));
			return $state;
		}
	}

	public function update($data,$condition)
	{
		$state = $this->mongoci->_update($data, $condition);
		if (empty($state) || isset($state->error)) {
			return false;
		} else {
			if(isset($state['answered_questions'])) $state['answered_questions'] = json_encode($state['answered_questions']);
			$this->predis->add_set(array("key" => array("user-profile" => $state["msisdn"]), "data" => $state));
			return $state["_id"];
		}
	}

	public function get($condition,$fields=array()){
		$data = $this->mongoci->_get($condition,$fields);
		if (empty($data) || isset($data->error)){
			return false;
		}
		else{
			 $this->predis->get_set(array("key"=>array("user_profile"=>$data["club"],)));
		}

	}

    public static function msisdn_sanitizer($msisdn,$plus = false){
        $msisdn = trim($msisdn);
        $msisdn = str_replace('+','',$msisdn);

        if(preg_match('/^234/i',$msisdn) == true){
            $msisdn = '0'.substr($msisdn,3);
        }

        if(strlen($msisdn) == 11){
            $msisdn = '+234'.substr($msisdn,1);
        }else{
            if(strpos($msisdn,'+') == false)
                $msisdn = '+'.$msisdn;
        }

        if($plus == false) $msisdn = str_replace('+','',$msisdn);

        return $msisdn;
    }

	public function move_to_next_level($msisdn){
		$this->load->model("question_model");
		$user_profile = $this->user_model->check($msisdn);
		$answered_questions = json_decode($user_profile['answered_questions'], true);

		$questions_answered = count($this->user-$user_profile['answered_questions']);

		if ($questions_answered > 5){
			$this->user_profile->update(array($user_profile['level']=> ($user_profile['level']+1)));
			}
		elseif ($questions_answered > 15){
			$this->user_profile->update(array($user_profile['level']=> ($user_profile['level']+1)));

		}
		else{
			$this->user_profile->update(array($user_profile['level']=> ($user_profile['level']+1)));
		}
		}
}
