<?php
// Codeigniter access check, remove it for direct use
if( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

$config["app_name"] = "Soccer Trivial";

$config["rmq-msg"] = array(
    "reg-error"=>"Hi, Account creation failed because: {REASON}",
    "reg-success"=>"Congratulations {USERNAME}, You've successfully registered '{CLUB_NAME}' as your favorite club on Soccer-Trivial account.
                    Send BEGIN or use {USER_LINK} to get started!!!",
    "invalid-msg"=>"Hello, seems you've sent in a wrong keyword, send HELP to proceed.",
    "acc-exist"=>"Hello {USERNAME}, You already have an existing Soccer-Trivial account",
    "start-msg"=>"Hello {USERNAME}, You can send 'start' to {SHORTCODE} to begin the game",
    "reg-msg" => "Hello , seems you do not have an account with us. Please, register first before playing." ,
    'ques-start' => "You have requested to start getting questions. Get ready, your questions would be coming soon",
    "help-msg"=>"",
    "quit-confirm-msg"=>"",
    "quit-msg"=>"You have ended Soccer-Trivial.",
);
