<?php 
// Codeigniter access check, remove it for direct use
if( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

$config['mongo'] = array('host'=>'127.0.0.1',
                            'port'=>'27017','user'=>'',
                            'password'=>'',
                            'database'=>'soccer_trivial');

$config['mongo_indexes'] = array(
    'users'=>array(
        array('key'=>array(array('username'=>1),'unique'=>true)),
        array('key'=>array(array('loc'=>'2dsphere'),'name'=>'geo'))
    ),
    //'clubs'=>array(array('key'=>array('abbrev'=>true},'unique'=>true))
    'clubs'=> [
        [ 'key' => [ 'abbrev' => 1 ], 'unique' => true ],
    ],

    'questions' => array(
        array('key'=>array('question'=>1), 'unique' => true, 'expire'=>'' , )
    )

);

$config['mongo_models'] = array(
    'users'=> 'user_profile',
    'requests'=>'request_monitor',
    'clubs'=>'club_info',
    'questions' => 'question'
);


