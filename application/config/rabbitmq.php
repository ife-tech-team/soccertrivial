<?php
// Codeigniter access check, remove it for direct use
if( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

$config["rmq"] = array("host"=>"127.0.0.1",
                            "port"=>"5672",
                            "username"=>"guest",
                            "password"=>"guest");
$config["rmq-queue"] = array(
    "kannel-request" => "soccer-trivial-request",
    "reg-queue" => "soccer-trivial-registration",
    "msg-queue" => "soccer-trivial-messaging",
    "ques-queue" => "soccer-trivial-question",

);

$config["rmq-msg"] = array(
    "reg-error"=>"Hi, Account creation failed because: {REASON}",
    "reg-success"=>"Congratulations {USERNAME}, You've successfully registered '{CLUB_NAME}' as your favorite club on Soccer-Trivial account.
                    Send BEGIN or use {USER_LINK} to get started!!!",
    "invalid-msg"=>"Hello, seems you've sent in a wrong keyword, send HELP to proceed.",
    "acc-exist"=>"Hello {USERNAME}, You already have an existing Soccer-Trivial account.",
    "start-msg"=>"Hello {USERNAME}, You can send 'start' to {SHORTCODE} to begin the game",
    "reg-msg" => "Hello , seems you do not have an account with us. Please, register first before playing." ,
    "help-msg"=>"Hello, you can send your name,club and gender to {SHORTCODE} to register for Soccer-Trivial, BEGIN if you are registered to start playing,.
                    LEADERBOARD to check the list of the players with the highest points,QUIT to quit the current session.",
    "quit-msg"=>"You have successfully quited the Soccer-Trivial",
    "question-msg"=>"Q.{QUESTION_COUNTER}.Level{LEVEL_COUNTER}:{QUESTION}.Send {OPTIONS_AND_VALUE}",
    "lead_msg" => "The highest players in Soccer-Trivial are {USERNAME}, {POINTS} , {CLUB}",
    "score_msg"=> "Hello, {USERNAME}, you have answered {QUESTIONS} and have {SCORE}"
);
