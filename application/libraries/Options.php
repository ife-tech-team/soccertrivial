<?php
/**
 * Description of Options
 *
 * @author Oluwasegun
 */
class Options {
	public static $_auth 						= "administrator";
	public static $_targets 					= "targets";
	public static $_campaign					= "campaigns";
	public static $_account						= "accounts";
	public static $_admin						= "admin";

	public static $_participants				= "participants";


	public static $_settings					= "settings";



}

